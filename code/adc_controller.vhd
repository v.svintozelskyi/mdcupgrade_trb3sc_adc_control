library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.trb_net_std.all;

entity adc_controller is
    generic (
        F_INP_CLK_MHz   : INTEGER RANGE 17 TO 1000 := 200;
        CH1_CMD         : STD_LOGIC_VECTOR(15 downto 0) := x"0000";
        CH2_CMD         : STD_LOGIC_VECTOR(15 downto 0) := x"0000";
        CH3_CMD         : STD_LOGIC_VECTOR(15 downto 0) := x"0000";
        CH4_CMD         : STD_LOGIC_VECTOR(15 downto 0) := x"0000";
        TMP_CMD         : STD_LOGIC_VECTOR(15 downto 0) := x"F393";
        GEN_CONV        : std_logic := '0'
    ); 
    port (
        CLK       : in std_logic;
        RST       : in std_logic;
        BUS_RX    : in CTRLBUS_RX;
        BUS_TX    : out CTRLBUS_TX;

        ADC_CS    : out std_logic;
        ADC_MOSI  : out std_logic;
        ADC_MISO  : in  std_logic;
        ADC_CLK   : out std_logic
    );
end adc_controller;

architecture rtl of adc_controller is
    type RAM is array (0 to 4) of std_logic_vector(15 downto 0);
    type RESRAM is array (0 to 4) of std_logic_vector(11 downto 0);
    signal cmd_ram : RAM := (CH1_CMD, CH2_CMD, CH3_CMD, CH4_CMD, TMP_CMD);
    signal res_ram : RESRAM;

    signal mem_base : integer range 0 to 4;
    signal mem_offset : integer range 0 to 4;
    signal mem_addr : integer range 0 to 4;
    signal res_wr : std_logic;
    signal res_data : std_logic_vector(11 downto 0);

    signal spi_write : std_logic;
    signal spi_busy  : std_logic;
    signal spi_ack   : std_logic;
    signal spi_unk   : std_logic;
    signal spi_addr  : std_logic;
    signal spi_data  : std_logic_vector(2 downto 0);
    signal spi_busy_fsm: std_logic;
    signal spi_cmd_data : std_logic_vector(15 downto 0);

    signal sc_ack   : std_logic;
    signal sc_nack  : std_logic;
    signal sc_unk   : std_logic;
    signal sc_spi   : std_logic;
    signal sc_data  : std_logic_vector(31 downto 0);
    signal sc_delayed_data  : std_logic_vector(31 downto 0);

    signal RST_SIG : std_logic;
    signal rst_sc : std_logic;

    TYPE fsm_sc_t IS (IDLE, WAIT_SPI);
    signal sc_fsm : fsm_sc_t;

    signal SPI_DEBUG : std_logic_vector(31 downto 0);

    signal temp_hrf_int : std_logic_vector(11 downto 0);
    signal temp_hrf_float : std_logic_vector(11 downto 0);
    signal temp_hrf_intpart : integer range 0 to 127;
    signal temp_hrf_i3 : integer range 0 to 1;
    signal temp_hrf_i2 : integer range 0 to 9;
    signal temp_hrf_i1 : integer range 0 to 9;

    type TEMP_CONV_ROM_T is array (0 to 7) of std_logic_vector(11 downto 0); 
    constant temp_conv_rom : TEMP_CONV_ROM_T := (
        0 => x"000",
        1 => x"125",
        2 => x"250",
        3 => x"375",
        4 => x"500",
        5 => x"625",
        6 => x"750",
        7 => x"875",
        others =>  x"FFF"
    );
begin
    
    mem_addr <= mem_base + mem_offset;
    RST_SIG <= rst_sc or RST;
    spi_cmd_data <= cmd_ram(mem_addr);

    TEMP_CONV_GEN: if GEN_CONV = '1' generate
        TEMP_CONVERSION: process
            variable intpart : integer range 0 to 99;
        begin
            wait until rising_edge(CLK);
            temp_hrf_intpart <= to_integer(unsigned(res_ram(4)(11 downto 3)));

            if temp_hrf_intpart >= 100 then
                temp_hrf_i3 <= 1;
            else
                temp_hrf_i3 <= 0;
            end if;
            
            if temp_hrf_i3 = 1 then
                intpart := temp_hrf_intpart - 100;
            else
                intpart := temp_hrf_intpart;
            end if;

            if intpart >= 90 then
                temp_hrf_i2 <= 9;
                intpart := intpart - 90;
            elsif intpart >= 80 then
                temp_hrf_i2 <= 8;
                intpart := intpart - 80;
            elsif intpart >= 70 then
                temp_hrf_i2 <= 7;
                intpart := intpart - 70;
            elsif intpart >= 60 then
                temp_hrf_i2 <= 6;
                intpart := intpart - 60;
            elsif intpart >= 50 then
                temp_hrf_i2 <= 5;
                intpart := intpart - 50;
            elsif intpart >= 40 then
                temp_hrf_i2 <= 4;
                intpart := intpart - 40;
            elsif intpart >= 30 then
                temp_hrf_i2 <= 3;
                intpart := intpart - 30;
            elsif intpart >= 20 then
                temp_hrf_i2 <= 2;
                intpart := intpart - 20;
            elsif intpart >= 10 then
                temp_hrf_i2 <= 1;
                intpart := intpart - 10;
            else
                temp_hrf_i2 <= 0;
            end if;

            temp_hrf_i1 <= intpart;

            temp_hrf_int <= std_logic_vector(to_unsigned(temp_hrf_i3, 4)) & std_logic_vector(to_unsigned(temp_hrf_i2, 4)) & std_logic_vector(to_unsigned(temp_hrf_i1, 4));
            temp_hrf_float <= temp_conv_rom(to_integer(unsigned(res_ram(4)(2 downto 0))));
        end process;
    end generate TEMP_CONV_GEN;
    TEMP_CONV_NOGEN: if GEN_CONV = '0' generate
        temp_hrf_int <= (others => '1');
        temp_hrf_float <= (others => '1');
    end generate TEMP_CONV_NOGEN;

    THE_SPI: entity work.spi_ltc2600_adc_controller
        generic map(
            F_INP_CLK_MHz => F_INP_CLK_MHz
        )port map(
            CLK_IN => CLK,
            RESET_IN => RST_SIG,
            -- Slave bus
            BUS_WRITE_IN => spi_write,
            BUS_BUSY_OUT => spi_busy,
            BUS_ACK_OUT => spi_ack,
            BUS_UNK_OUT => spi_unk,
            BUS_ADDR_IN => spi_addr,
            BUS_DATA_IN => spi_data,
            -- SPI connections
            SPI_CS_OUT => ADC_CS,
            SPI_SDI_IN => ADC_MISO,
            SPI_SDO_OUT => ADC_MOSI,
            SPI_SCK_OUT => ADC_CLK,
            
            SPI_BUSY    => spi_busy_fsm,
            
            -- EXTERNAL RAM
            RAM_OFFSET  => mem_offset,
            CMD_DATA    => spi_cmd_data,
            RES_DATA    => res_data,
            RES_WRITE   => res_wr,

            DEBUG => SPI_DEBUG
        );

    RES_MEM : process
    begin
        wait until rising_edge(CLK);
        if res_wr = '1' then
            res_ram(mem_addr) <= res_data;
        end if;
    end process;

    
    SLOW_CONTROL_TRANSM : process
    begin
        wait until rising_edge(CLK);
        BUS_TX.ack      <= '0';
        BUS_TX.unknown  <= '0';
        BUS_TX.nack     <= '0';
        BUS_TX.data     <= (others => '0');

        case sc_fsm is
            when IDLE =>
                if sc_ack = '1' then
                    BUS_TX.ack      <= '1';
                    BUS_TX.data     <= sc_data;
                elsif sc_unk = '1' then
                    BUS_TX.unknown  <= '1';
                elsif sc_nack = '1' then
                    BUS_TX.nack  <= '1';
                elsif sc_spi = '1' then
                    sc_fsm <= WAIT_SPI;
                    sc_delayed_data <= sc_data;
                end if;
            when WAIT_SPI =>
                if or_all((spi_busy, spi_unk, spi_ack)) = '1' then
                    sc_fsm <= IDLE;
                    if spi_ack = '1' then
                        BUS_TX.ack       <= '1';
                        BUS_TX.data      <= sc_delayed_data;
                    elsif spi_busy = '1' then
                        BUS_TX.nack      <= '1';
                    elsif spi_unk = '1' then
                        BUS_TX.unknown   <= '1';
                    end if;
                end if;
            when others =>
                sc_fsm <= IDLE;    
        end case;
        if RST_SIG = '1' then
            sc_fsm <= IDLE;
        end if;
    end process;

    SLOW_CONTROL_REC : process
    begin
        wait until rising_edge(CLK);
        sc_ack <= '0';
        sc_nack <= '0';
        sc_unk <= '0';
        sc_spi <= '0';
        rst_sc <= '0';
        sc_data <= (others => '0');

        spi_write <= '0';
        spi_addr <= '0';
        spi_data <= (others => '0');


        -- address space 0xd480 - 0xd49f
        if sc_fsm = IDLE then 
            if BUS_RX.write = '1' then
                if BUS_RX.addr(3 downto 0) = x"0" then
                    rst_sc <= '1';
                    sc_ack <= '1';
                elsif BUS_RX.addr(3 downto 0) = x"7" then
                    sc_ack <= '1';
                    cmd_ram(0) <= BUS_RX.data(15 downto 0);
                    cmd_ram(1) <= BUS_RX.data(31 downto 16);
                elsif BUS_RX.addr(3 downto 0) = x"8" then
                    sc_ack <= '1';
                    cmd_ram(2) <= BUS_RX.data(15 downto 0);
                    cmd_ram(3) <= BUS_RX.data(31 downto 16);
                elsif BUS_RX.addr(3 downto 0) = x"9" then
                    sc_ack <= '1';
                    cmd_ram(4) <= BUS_RX.data(15 downto 0);
                elsif BUS_RX.addr(3) = '1' then
                    if spi_busy_fsm = '1' then
                        sc_ack<= '0';
                        sc_nack <= '1';
                    else
                        if BUS_RX.addr(3 downto 0) = x"A" then
                            sc_spi <= '1';
                            mem_base <= 0;
                            spi_write <= '1';
                            spi_addr <= '1';
                            spi_data <= "001";
                        elsif BUS_RX.addr(3 downto 0) = x"B" then
                            sc_spi <= '1';
                            mem_base <= 1;
                            spi_write <= '1';
                            spi_addr <= '1';
                            spi_data <= "001";
                        elsif BUS_RX.addr(3 downto 0) = x"C" then
                            sc_spi <= '1';
                            mem_base <= 2;
                            spi_write <= '1';
                            spi_addr <= '1';
                            spi_data <= "001";
                        elsif BUS_RX.addr(3 downto 0) = x"D" then
                            sc_spi <= '1';
                            mem_base <= 3;
                            spi_write <= '1';
                            spi_addr <= '1';
                            spi_data <= "001";
                        elsif BUS_RX.addr(3 downto 0) = x"E" then
                            sc_spi <= '1';
                            mem_base <= 4;
                            spi_write <= '1';
                            spi_addr <= '1';
                            spi_data <= "001";
                        elsif BUS_RX.addr(3 downto 0) = x"F" then
                            sc_spi <= '1';
                            mem_base <= 0;
                            spi_write <= '1';
                            spi_addr <= '1';
                            spi_data <= "101";
                        end if;
                    end if;
                else
                    sc_unk  <=   '1';
                end if;
            elsif BUS_RX.read = '1' then
                sc_ack <= '1';
                if BUS_RX.addr(3 downto 0) = x"1" then
                    sc_data <= x"0" & res_ram(1) & x"0" & res_ram(0); 
                elsif BUS_RX.addr(3 downto 0) = x"2" then
                    sc_data <= x"0" & res_ram(3) & x"0" &  res_ram(2);
                elsif BUS_RX.addr(3 downto 0) = x"3" then
                    sc_data <= x"00000" & res_ram(4);
                elsif BUS_RX.addr(3 downto 0) = x"4" then
                    sc_data <= x"0" & res_ram(1) & x"0" & res_ram(0); 
                    sc_ack <= '0';
                    sc_spi <= '1';
                    mem_base <= 0;
                    spi_write <= '1';
                    spi_addr <= '1';
                    spi_data <= "101";
                elsif BUS_RX.addr(3 downto 0) = x"5" then
                    sc_data <= x"0" & res_ram(3) & x"0" & res_ram(2); 
                    sc_spi <= '1';
                    sc_ack <= '0';
                    mem_base <= 0;
                    spi_write <= '1';
                    spi_addr <= '1';
                    spi_data <= "101";
                elsif BUS_RX.addr(3 downto 0) = x"6" then
                    sc_data <= x"00000" & res_ram(4);
                    sc_spi <= '1';
                    sc_ack <= '0';
                    mem_base <= 0;
                    spi_write <= '1';
                    spi_addr <= '1';
                    spi_data <= "101";
                elsif BUS_RX.addr(3 downto 0) = x"7" then
                    sc_data <= cmd_ram(1) & cmd_ram(0);
                elsif BUS_RX.addr(3 downto 0) = x"8" then
                    sc_data <= cmd_ram(3) & cmd_ram(2);
                elsif BUS_RX.addr(3 downto 0) = x"9" then
                    sc_data <= x"0000" & cmd_ram(4);
                    
                elsif BUS_RX.addr(3 downto 0) = x"A" then
                    sc_data <= x"00000" & res_ram(0); 
                elsif BUS_RX.addr(3 downto 0) = x"B" then
                    sc_data <= x"00000" & res_ram(1);
                elsif BUS_RX.addr(3 downto 0) = x"C" then
                    sc_data <= x"00000" & res_ram(2);
                elsif BUS_RX.addr(3 downto 0) = x"D" then
                    sc_data <= x"00000" & res_ram(3);
                elsif BUS_RX.addr(3 downto 0) = x"E" then
                    sc_data <= x"0" & temp_hrf_int & temp_hrf_float & x"0";
                else
                    sc_ack  <=   '0';
                    sc_unk  <=   '1';
                end if;
            end if;
        end if;
    end process;
    
end architecture;