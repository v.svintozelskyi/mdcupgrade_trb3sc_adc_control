library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.config.all;
use work.trb_net_std.all;
use work.trb_net_components.all;

entity fee_signals is
  port(
    CLK   : in std_logic;
    RESET : in std_logic;
    
    BUS_RX : in CTRLBUS_RX;
    BUS_TX : out CTRLBUS_TX;
    
    TEST_SIG_OUT : out   std_logic_vector( 7 downto 0);
    FEETEMP      : inout std_logic_vector( 3 downto 0)
    );
end entity;



architecture arch of fee_signals is

signal timer_reg  : std_logic_vector(11 downto 0) := x"3FF";
signal select_reg, invert_reg : std_logic_vector(7 downto 0);

type temp_arr is array(0 to 3) of std_logic_vector(11 downto 0);
signal temps : temp_arr;

signal timer : unsigned(11 downto 0);
signal temp_i   : std_logic_vector(11 downto 0);
signal status_i : std_logic_vector(31 downto 0);

signal onewire_in, onewire_out : std_logic;
signal finished, lastread_q : std_logic;
signal onewireselect : integer range 0 to 3;
signal start : std_logic;

begin

PROC_REG : process begin
  wait until rising_edge(CLK);
  BUS_TX.ack     <= '0';
  BUS_TX.nack    <= '0';
  BUS_TX.unknown <= '0';
  start <= '0';
  
  if BUS_RX.read = '1' then
    BUS_TX.ack     <= '1';
    if BUS_RX.addr(4 downto 0) = "00000" then
      BUS_TX.data <= x"00" & invert_reg & x"00" & select_reg;
    elsif BUS_RX.addr(4 downto 0) = "00001" then
      BUS_TX.data <= x"00000" & timer_reg;
    elsif BUS_RX.addr(4 downto 0) = "10000" then
      BUS_TX.data <= x"00000" & temps(0);
    elsif BUS_RX.addr(4 downto 0) = "10001" then
      BUS_TX.data <= x"00000" & temps(1);
    elsif BUS_RX.addr(4 downto 0) = "10010" then
      BUS_TX.data <= x"00000" & temps(2);
    elsif BUS_RX.addr(4 downto 0) = "10011" then
      BUS_TX.data <= x"00000" & temps(3);
    else
      BUS_TX.ack     <= '0';
      BUS_TX.unknown <= '1';
    end if;  
  end if;  
  if BUS_RX.write = '1' then
    BUS_TX.ack <= '1';
    if BUS_RX.addr(4 downto 0) = "00000" then
      select_reg <= BUS_RX.data(7 downto 0);
      invert_reg <= BUS_RX.data(23 downto 16);
      start <= '1';
    elsif BUS_RX.addr(4 downto 0) = "00001" then
      timer_reg <= BUS_RX.data(11 downto 0);
    else  
      BUS_TX.ack     <= '0';
      BUS_TX.unknown <= '1';
    end if;      
  end if;
  
end process;

THE_ONEWIRE : trb_net_onewire 
  generic map(
    USEINOUT => c_NO
    )
  port map(
    CLK      => CLK,
    RESET    => RESET,
    READOUT_ENABLE_IN => '1',
    
    ONEWIRE_IN  => onewire_in,
    ONEWIRE_OUT => onewire_out,
    MONITOR_OUT => open,
    --connection to id ram, according to memory map in TrbNetRegIO
    DATA_OUT => open,
    ADDR_OUT => open,
    WRITE_OUT=> open,
    TEMP_OUT => temp_i,
    ID_OUT   => open,
    STAT     => status_i
    );
    
    
PROC_ONEWIRE : process(onewire_out,onewireselect)
begin
  FEETEMP <= (others => 'Z');
  if onewire_out = '0' then
    FEETEMP(onewireselect) <= '0';
  end if;  
end process;
  
  onewire_in <= FEETEMP(onewireselect);

PROC_ONEWIRESELECT : process begin
  wait until rising_edge(CLK);
  lastread_q <= status_i(6);
  finished <= lastread_q and not status_i(6);
  
  if finished = '1' then
    temps(onewireselect) <= temp_i;
    onewireselect <= onewireselect + 1;
  end if;
end process;
  
PROC_TESTSIG : process begin
  wait until rising_edge(CLK);
  if start = '1' then
    timer <= unsigned(timer_reg);
    TEST_SIG_OUT <= (invert_reg xor select_reg);
  elsif timer > 0 then
    timer <= timer - 1 ;
  else
    TEST_SIG_OUT <= invert_reg;
  end if;
  
end process;
  
end architecture;
