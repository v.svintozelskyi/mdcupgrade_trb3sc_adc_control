LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY work;
USE work.trb_net_std.ALL;

ENTITY spi_ltc2600_adc_controller IS
    GENERIC (
        F_INP_CLK_MHz : INTEGER RANGE 17 TO 1000 := 200
    );
    PORT (
        CLK_IN : IN STD_LOGIC;
        RESET_IN : IN STD_LOGIC;
        -- Slave bus
        BUS_WRITE_IN : IN STD_LOGIC;
        BUS_BUSY_OUT : OUT STD_LOGIC;
        BUS_ACK_OUT : OUT STD_LOGIC;
        BUS_UNK_OUT : OUT STD_LOGIC;
        BUS_ADDR_IN : IN STD_LOGIC;
        BUS_DATA_IN : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        -- SPI connections
        SPI_CS_OUT : OUT STD_LOGIC;
        SPI_SDI_IN : IN STD_LOGIC;
        SPI_SDO_OUT : OUT STD_LOGIC;
        SPI_SCK_OUT : OUT STD_LOGIC;

        SPI_BUSY : OUT STD_LOGIC;
        -- EXTERNAL RAM
        RAM_OFFSET : OUT INTEGER RANGE 0 TO 7;
        CMD_DATA : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        RES_DATA : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
        RES_WRITE : OUT STD_LOGIC;

        DEBUG: OUT std_logic_vector(31 downto 0)
    );
END ENTITY;
ARCHITECTURE spi_ltc2600_arch OF spi_ltc2600_adc_controller IS

    SIGNAL start : STD_LOGIC;
    -- SIGNAL reset_fsm : STD_LOGIC;

    SIGNAL spi_cs : STD_LOGIC;
    SIGNAL spi_sck : STD_LOGIC;
    SIGNAL spi_sdo : STD_LOGIC;
    SIGNAL spi_sdi : STD_LOGIC;

    SIGNAL readback : STD_LOGIC_VECTOR(15 DOWNTO 0);

    TYPE fsm_t IS (IDLE, WAIT_STATE, SET, TOGGLE_CS, TOGGLE_CS_0, TOGGLE_CS_1, WAIT_DAQ, DAQ, DAQ2, FINISH_WAIT_1, FINISH_WAIT_2, FINISH_WORD, FINISH);
    SIGNAL fsm_state : fsm_t;

    CONSTANT word_length : INTEGER := 16;
    CONSTANT wait_cycles : INTEGER := (F_INP_CLK_MHz - 16) / 8;

    SIGNAL time_count : INTEGER RANGE 0 TO 1023;--8191;--2047;--1023;
    SIGNAL word_count : INTEGER RANGE 0 TO 7;
    SIGNAL bit_count : INTEGER RANGE 0 TO word_length-1;

    signal ram_off :integer range 0 to 7;
BEGIN

    -- RES_DATA <= readback; -- when CMD_DATA(11 downto 9) = "011" else           -- 1v
    --             -- (readback srl ) when CMD_DATA(11 downto 9) = "010" else           -- 2v
                -- (readback srl 3) when CMD_DATA(11 downto 9) = "001" else readback; -- 4v
    RAM_OFFSET <= ram_off;

    CONV_DATA: process(readback, CMD_DATA)
        variable value : unsigned(11 downto 0); 
    begin
        if CMD_DATA(4) = '0' then
            value := unsigned(readback(15 downto 4));
            -- adc mode
            if CMD_DATA(11 downto 9) = "011" then
                -- 1v
                RES_DATA <= std_logic_vector(shift_right(value,1)); --srl 5;
            elsif CMD_DATA(11 downto 9) = "010" then
                -- 2v
                RES_DATA <= std_logic_vector(value); --srl 5;
            elsif CMD_DATA(11 downto 9) = "001" then
                -- 4v
                RES_DATA <= std_logic_vector(shift_left(value,1)); --srl 5;
            else
                RES_DATA <= readback(15 downto 4);
            end if;            
        else
            -- temp mode
            RES_DATA <= readback(15 downto 4);
        end if;        
    end process;

    PROC_MEM : PROCESS
    BEGIN
        WAIT UNTIL rising_edge(CLK_IN);

        BUS_ACK_OUT <= '0';
        BUS_UNK_OUT <= '0';
        BUS_BUSY_OUT <= '0';

        -- reset_fsm <= '0';
        start <= '0';

        IF BUS_WRITE_IN = '1' THEN
            BUS_ACK_OUT <= '1';
            -- IF BUS_ADDR_IN = '0' THEN -- RESET
                -- reset_fsm <= '1';
            IF BUS_ADDR_IN = '1' THEN --0x11                           -- start transmitting
                IF fsm_state = IDLE THEN
                    word_count <= to_integer(unsigned(BUS_DATA_IN));
                    start <= '1';
                ELSE
                    BUS_ACK_OUT <= '0';
                    BUS_BUSY_OUT <= '1';
                END IF;
            ELSE
                BUS_UNK_OUT <= '1';
                BUS_ACK_OUT <= '0';
            END IF;
        END IF;
    END PROCESS;

    PROC_FSM : PROCESS
    BEGIN
        WAIT UNTIL rising_edge(CLK_IN);
        RES_WRITE <= '0';

        CASE fsm_state IS
            WHEN IDLE =>

                IF start = '1' THEN
                    ram_off <= 0;
                    bit_count <= word_length - 1;
                    time_count <= wait_cycles;
                    fsm_state <= WAIT_STATE;
                    spi_cs <= '1';
                    spi_sck <= '0';
                ELSE
                    spi_cs <= '0';
                    spi_sck <= '0';
                    spi_sdo <= '0';
                END IF;

            WHEN WAIT_STATE =>
                IF time_count = 0 THEN
                    fsm_state <= SET;
                ELSE
                    time_count <= time_count - 1;
                END IF;

            WHEN SET =>
                time_count <= wait_cycles;
                spi_sck <= NOT spi_sck;
                IF spi_sck = '0' THEN
                    spi_sdo <= CMD_DATA(bit_count);
                    IF bit_count /= 0 THEN
                        bit_count <= bit_count - 1;
                        fsm_state <= WAIT_STATE;
                    ELSE
                        fsm_state <= TOGGLE_CS;
                        time_count <= wait_cycles;
                    END IF;
                ELSE
                    fsm_state <= WAIT_STATE;
                END IF;
            WHEN TOGGLE_CS =>
                IF time_count = 0 AND spi_sck = '0' THEN
                    time_count <= wait_cycles;
                    spi_cs <= '0';
                    spi_sdo <= '0';
                    fsm_state <= TOGGLE_CS_0;
                ELSIF time_count = 0 AND spi_sck = '1' THEN
                    spi_sck <= NOT spi_sck; -- 1 -> 0
                    time_count <= wait_cycles;
                ELSE
                    time_count <= time_count - 1;
                END IF;
            WHEN TOGGLE_CS_0 => -- wait 1 period, cs high
                IF time_count = 0 THEN
                    fsm_state <= TOGGLE_CS_1;
                    time_count <= wait_cycles;
                ELSE
                    time_count <= time_count - 1;
                END IF;
            WHEN TOGGLE_CS_1 => -- wait 1 period, cs high
                IF time_count = 0 THEN
                    fsm_state <= WAIT_DAQ;
                    time_count <= wait_cycles;
                    spi_cs <= '1';
                ELSE
                    time_count <= time_count - 1;
                END IF;
            WHEN WAIT_DAQ =>
                IF time_count = 0 THEN
                    time_count <= wait_cycles;
                    IF (spi_sdi = '0') THEN
                        fsm_state <= DAQ;
                        bit_count <= word_length - 1;
                        readback <= x"0000";
                        -- readback2 <= x"0000";
                    END IF;
                ELSE
                    time_count <= time_count - 1;
                END IF;

                -- DEBUG(7 downto 0) <= std_logic_vector(to_unsigned(bit_count, 8));
                -- DEBUG(8) <= spi_sdi;
                -- DEBUG(9) <= spi_cs;
                -- DEBUG(10) <= SPI_SDI_IN;
                -- DEBUG(31 downto 16) <= readback;
            WHEN DAQ =>
                IF time_count = 0 AND spi_sck = '0' THEN
                    time_count <= wait_cycles;
                    spi_sck <= NOT spi_sck; -- 0 -> 1    
                ELSIF time_count = 0 AND spi_sck = '1' THEN
                    spi_sck <= NOT spi_sck; -- 1 -> 0
                    time_count <= wait_cycles;
                    
                    -- report "Reading bit# " & integer'image(bit_count);
                    readback(bit_count) <= spi_sdi;
                    IF (bit_count = 0) THEN
                        fsm_state <= FINISH_WAIT_1;
                        -- bit_count <= word_length - 1;
                    ELSE
                        bit_count <= bit_count - 1;
                    END IF;
                ELSE
                    time_count <= time_count - 1;
                END IF;

            -- WHEN DAQ2 =>
            --     IF time_count = 0 AND spi_sck = '0' THEN
            --         time_count <= wait_cycles;
            --         spi_sck <= NOT spi_sck; -- 0 -> 1   
            --         readback_conf_ri(bit_count) <= spi_sdi; 
            --     ELSIF time_count = 0 AND spi_sck = '1' THEN
            --         spi_sck <= NOT spi_sck; -- 1 -> 0
            --         time_count <= wait_cycles;
            --         readback2(bit_count) <= spi_sdi;
            --         IF (bit_count = 0) THEN
            --             fsm_state <= FINISH_WAIT_1;
            --         ELSE
            --             bit_count <= bit_count - 1;
            --         END IF;
            --     ELSE
            --         time_count <= time_count - 1;
            --     END IF;

            WHEN FINISH_WAIT_1 => -- wait 1 period, cs high
                IF time_count = 0 THEN
                    fsm_state <= FINISH_WAIT_2;
                    time_count <= wait_cycles;
                    spi_cs <= '0';

                    RES_WRITE <= '1';

                    -- DEBUG(31 downto 16) <= readback_conf_ri;
                    -- DEBUG(15 downto 0) <= readback2;
                ELSE
                    time_count <= time_count - 1;
                END IF;
            WHEN FINISH_WAIT_2 => -- wait 1 period, cs high
                IF time_count = 0 THEN
                    fsm_state <= FINISH_WORD;
                    time_count <= wait_cycles;
                ELSE
                    time_count <= time_count - 1;
                END IF;

            WHEN FINISH_WORD => -- wait 1 period, cs high
                IF time_count = 0 THEN
                    bit_count <= word_length - 1;
                    IF ram_off /= word_count - 1 THEN
                        time_count <= wait_cycles;
                        fsm_state <= WAIT_STATE;
                        spi_cs <= '1';
                        spi_sck <= '0';
                        ram_off <= ram_off + 1;
                    ELSE
                        fsm_state <= FINISH;
                        time_count <= 0;
                    END IF;
                ELSE
                    time_count <= time_count - 1;
                END IF;

            WHEN FINISH =>
                spi_sck <= '0';
                spi_sdo <= '0';
                spi_cs <= '0';
                fsm_state <= IDLE;

            WHEN others =>
                fsm_state <= IDLE;
                spi_cs <= '0';
                spi_sck <= '0';
                spi_sdo <= '0';
        END CASE;
        IF RESET_IN = '1' THEN
            fsm_state <= IDLE;
            spi_cs <= '0';
            spi_sck <= '0';
            spi_sdo <= '0';
            -- readback <= (OTHERS => '0');
        END IF;
    END PROCESS;

    -- Outputs and Inputs
    SPI_BUSY <= '1' WHEN fsm_state /= IDLE ELSE '0';

    spi_sdi <= SPI_SDI_IN;--(SPI_SDI_IN AND spi_cs);

    SPI_CS_OUT <= NOT spi_cs;
    SPI_SDO_OUT <= spi_sdo;-- AND spi_cs;
    SPI_SCK_OUT <= spi_sck;

END ARCHITECTURE;