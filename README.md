
# adc_controller

VHDL module for ADC data collection and configuration. Compatible ADC: 12 bit ADS1018.

Top level module for ADC control: 

> code/adc_controller.vhd

## Hierarchy 

* adc_controller.vhd
  + *THE_SPI*:    **spi_ltc2600_adc_controller.vhd**   - updated version of spi_ltc2600.vhd                               

## Top-level module instantiation

### Generic map
| Name        | Type        								| Default |  Description                         |
| ----------- | ----------  								| --- 			| ----------------------------------- |
| F_INP_CLK_MHz | integer range 17 to 1000 	| 200			| Clk frequency in MHz		|
| CH1_CMD  | std_logic_vector(15 downto 0) | 0x0000 | ADC settings for 1 channel |
| CH2_CMD  | std_logic_vector(15 downto 0) | 0x0000 | ADC settings for 2 channel |
| CH3_CMD  | std_logic_vector(15 downto 0) | 0x0000 | ADC settings for 3 channel |
| CH4_CMD  | std_logic_vector(15 downto 0) | 0x0000 | ADC settings for 4 channel |
| TMP_CMD  | std_logic_vector(15 downto 0) | 0xF393 | ADC settings for temperature channel |
| GEN_CONV   | std_logic | 0 | Generate decimal temperature conversion module (1 - yes, 0 - no) |

### Port  map
| Name        | Type       | Mode | Description                         |
| ----------- | ---------- | ---- | ----------------------------------- |
| CLK         | std_logic  | In   | Clock signal                        |
| RST         | std_logic  | In   | Reset signal                        |
| BUS_RX      | CTRLBUS_RX | In   | Slow control input                  |
| BUS_TX      | CTRLBUS_TX | Out  | Slow control output                 |
| ADC_CS      | std_logic | Out  | ADC SPI chip-select signal                 |
| ADC_MOSI      | std_logic | Out  | ADC SPI MOSI signal                 |
| ADC_MISO      | std_logic | In  | ADC SPI MISO signal                 |
| ADC_CLK      | std_logic | Out  | ADC SPI clock signal                 |


## Slow control

### General rules

* For every command, the module should respond with either ack or nack or unknown bit not later than 5 clock cycles.

* If second commands arrives earlier than module send response for first one, it'll be ignored.

### Protocol

| BUS_RX.addr | Access mode | BUX_RX.data (only in W mode) | BUX_TX.data | Description                                                  |
| ------------------------------------------------- | ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **0xD480**  | W | --- | --- | Reset (independent of whole system) |
| **0xD481**  | R | --- | *11..0* - value of channel 1 in mV, unsigned <br>*27..16* - value of channel 2 in mV, unsigned | Read stored value of channels 1 & 2 |
| **0xD482**  | R | --- | *11..0* - value of channel 3 in mV, unsigned <br>*27..16* - value of channel 4 in mV, unsigned | Read stored value of channels 3 & 4 |
| **0xD483**  | R | --- | *11..0* - value of temperature in 2^-3 *C, unsigned | Read stored value of temperature |
| **0xD484**  | R | --- | *11..0* - value of channel 1 in mV, unsigned <br>*27..16* - value of channel 2 in mV, unsigned | Read stored value of channels 1 & 2 and trigger new data aqusition |
| **0xD485**  | R | --- | *11..0* - value of channel 3 in mV, unsigned <br>*27..16* - value of channel 4 in mV, unsigned | Read stored value of channels 3 & 4 and trigger new data aqusition |
| **0xD486**  | R | --- | *11..0* - value of temperature in 2^-3 *C, unsigned | Read stored value of temperature and trigger new data aqusition |
| **0xD487**  | R/W | *15..0* - configuration for channel 1 <br> *31..16* - configuration for channel 2 | *15..0* - configuration for channel 1 <br> *31..16* - configuration for channel 2 | Access to configuration for channels 1 & 2 |
| **0xD488**  | R/W | *15..0* - configuration for channel 1 <br> *31..16* - configuration for channel 2 | *15..0* - configuration for channel 3 <br> *31..16* - configuration for channel 4 | Access to configuration for channels 3 & 4 |
| **0xD489**  | R/W | *15..0* - configuration for temperature | *15..0* - configuration for temperature | Access to configuration for temperature |
| **0xD48A**  | W | --- | --- | Trigger DAQ for channel 1 |
| **0xD48B**  | W | --- | --- | Trigger DAQ for channel 2 |
| **0xD48C**  | W | --- | --- | Trigger DAQ for channel 3 |
| **0xD48D**  | W | --- | --- | Trigger DAQ for channel 4 |
| **0xD48E**  | W | --- | --- | Trigger DAQ for temperature |
| **0xD48F**  | W | --- | --- | Trigger full system DAQ |

Next functions is availible if and only if GEN_CONV = '1':


| BUS_RX.addr | Access mode | BUX_RX.data (only in W mode) | BUX_TX.data | Description                                                  |
| ------------------------------------------------- | ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **0xD48A**  | R | --- | **11..0** - value of channel 1 in mV, unsigned | Read channel 1 in human readable format |
| **0xD48B**  | R | --- | **11..0** - value of channel 2 in mV, unsigned | Read channel 2 in human readable format |
| **0xD48C**  | R | --- | **11..0** - value of channel 3 in mV, unsigned | Read channel 3 in human readable format |
| **0xD48D**  | R | --- | **11..0** - value of channel 4 in mV, unsigned | Read channel 4 in human readable format |
| **0xD48E**  | R | --- | **31..0** - decimal representation of temperature. First 4 hexadecimal digets means integer part. For example 0x00385000 means that the temperature is 38.5 degrees | Read temperature in human readable format |


## Configuration of channels

The configuration for each channel is used "as is" - directly send to ADC during DAQ. Detail description of format for this configurations can be found in the [datasheet](https://www.ti.com/lit/ds/symlink/ads1018.pdf?ts=1634631903743&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FADS1018). 

## Reading data from ADC

To read ADC value it's required to send two slow control requests:
* **0xD48A** - **0xD48F**, **0xD484** - **0xD486** - trigger data aqusition.
* **0xD481** - **0xD486** - read the measured value.

## Files

Code:

> code/adc_controller.vhd

> code/spi_ltc2600_adc_controller.vhd

