TOPNAME                      => "trb3sc_richSensor",
lm_license_file_for_synplify => "7788\@fb07pc-u102325",
lm_license_file_for_par      => "7788\@fb07pc-u102325",
lattice_path                 => '/usr/local/diamond/3.11_x64/',
synplify_path                => '/usr/local/diamond/3.11_x64/synpbase',
synplify_command             => "synpwrap -fg -options",
#synplify_command             => "ssh  adrian\@jspc37.x-matter.uni-frankfurt.de \"cd /local/adrian/git/dirich/combiner_cts/; LM_LICENSE_FILE=27020\@jspc29 /d/jspc29/lattice/synplify/O-2018.09-SP1/bin/synplify_premier -batch combiner.prj\"",

nodelist_file                => '../nodes_lxhadeb07.txt',
#par_options                  => '../par.p2t',

#Include only necessary lpf files
pinout_file                  => 'trb3sc_richSensor', #name of pin-out file, if not equal TOPNAME

#Report settings
firefox_open                 => 0,
#no_ltxt2ptxt                 => 1,  #if there is no serdes being used
#make_jed                     => 1,

