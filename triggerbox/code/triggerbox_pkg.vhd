library ieee;
USE IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;
use work.trb_net_std.all;

package triggerbox_pkg is

  type CONF_input_processor_t is record
    async         : std_logic;
    enable        : std_logic;
    invert        : std_logic;
    edge_detect   : std_logic;
    downscale     : std_logic_vector(3 downto 0);
    stretch_unit  : std_logic_vector(1 downto 0);
    stretch_value : std_logic_vector(11 downto 0);
    delay         : std_logic_vector(9 downto 0);
  end record;

  
  type CONF_coincidence_t is record
    mux_input1   : integer range 0 to 255;
    mux_input2   : integer range 0 to 255;
    mux_input3   : integer range 0 to 255;
    invert       : std_logic_vector(2 downto 0);
    enable       : std_logic_vector(2 downto 0);
    module_enable: std_logic;
  end record;  

  type CONF_multiplicity_t is record
    multiplicity : unsigned(4 downto 0);
    enable       : std_logic_vector(95 downto 0);
  end record;    
  
end;

package body triggerbox_pkg is


end package body;
