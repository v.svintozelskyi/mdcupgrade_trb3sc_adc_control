library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;
  use work.config.all;
  use work.triggerbox_pkg.all;


entity groups is
  generic (
    NUM_INPUTS : integer range 0 to 256 := 128
    );
  port (
    CLK      : in  std_logic;
    INP      : in  std_logic_vector(NUM_INPUTS-1 downto 0);  
    OUTP     : out std_logic;
    CONF     : in  std_logic_vector(NUM_INPUTS-1 downto 0)
    );
end entity;


architecture arch of groups is
attribute syn_hier : string;
attribute syn_hier of arch : architecture is "fixed";

attribute HGROUP: string;
attribute BBOX: string;
attribute HGROUP of arch : architecture is "groups";
attribute BBOX   of arch: architecture is "5,5";


begin


  OUTP <= or (CONF and INP) when rising_edge(CLK);


end architecture;
