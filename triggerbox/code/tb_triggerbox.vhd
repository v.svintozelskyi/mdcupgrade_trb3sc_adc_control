library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;
  use work.config.all;
  use work.triggerbox_pkg.all;
entity tb is
end entity;

architecture arch of tb is

signal clk_100, clk_200, reset : std_logic := '1';
signal inputs : std_logic_vector(7 downto 0);
signal busrx : CTRLBUS_RX;

signal test : std_logic;
begin

reset <= '0' after 20 ns;
clk_200 <= not clk_200 after 2.5 ns;
clk_100 <= not clk_100 after 5 ns;

inputs(7 downto 1) <= (others => '0');
inputs(0) <= '0', '1' after 254 ns, '0' after 273 ns, '0' after 500 ns;


THE_DUT : entity work.triggerbox
  generic map(
    NUM_INPUTS  => 8,
    NUM_VIRTUALIN => 8,
    NUM_OUTPUTS => 2,
    NUM_GROUPS  => 2,
    NUM_COINC   => 2,
    NUM_MULT    => 2
    )
  port map(
    CLK_SYS     => clk_100,
    CLK_FULL    => clk_200,
    RESET       => reset,
    
    INP         => inputs,
    OUTP        => open,

    BUS_RX  => busrx,
    BUS_TX  => open,
    
    BUSRDO_RX => ('0','0','0','0',(others => '0'),(others => '0'),(others => '0'),(others => '0'),(others => '0'),'0','0','0','0','0','0'),
    BUSRDO_TX => open
    );
    
    
end architecture;
