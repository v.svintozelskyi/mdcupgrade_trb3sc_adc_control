library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;
  use work.config.all;
  use work.triggerbox_pkg.all;


entity monitor_output is
  generic (
    NUM_INPUTS : integer range 1 to 512 := 32
    );
  port (
    CLK  : in  std_logic;
    INP  : in  std_logic_vector(NUM_INPUTS-1 downto 0);
    OUTP : out std_logic;
    CONF : in  integer range 0 to 511
    );
end entity;


architecture arch of monitor_output is

attribute syn_hier : string;
attribute syn_hier of arch : architecture is "fixed";

attribute HGROUP: string;
attribute HGROUP of arch : architecture is "monitor_output";

signal reg_inp : std_logic_vector(INP'range);

begin

  reg_inp <= INP when rising_edge(CLK);
  OUTP <= reg_inp(CONF) when rising_edge(CLK);
  

end architecture;



