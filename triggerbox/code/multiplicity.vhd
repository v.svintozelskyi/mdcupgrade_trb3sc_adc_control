library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;
  use work.config.all;
  use work.triggerbox_pkg.all;


entity multiplicity is
  generic (
    NUM_INPUTS : integer range 1 to 96 := 96
    );
  port (
    CLK      : in  std_logic;
    INP      : in  std_logic_vector(NUM_INPUTS-1 downto 0);
    OUTP     : out std_logic;
    CONF     : in  CONF_multiplicity_t
    );
end entity;


architecture arch of multiplicity is

attribute syn_hier : string;
attribute syn_hier of arch : architecture is "fixed";

attribute HGROUP: string;
attribute HGROUP of arch : architecture is "mult_proc";
attribute BBOX: string;
attribute BBOX   of arch: architecture is "10,10";

signal reg_inp : std_logic_vector(INP'range);
signal mult_gated : std_logic_vector(95 downto 0) := (others => '0');
type mult_arr is array(0 to 5) of unsigned(4 downto 0);
signal current_multiplicity : mult_arr;
signal out_reg : std_logic;
signal result : unsigned(4 downto 0);
  attribute syn_preserve : boolean;
  attribute syn_preserve of mult_gated : signal is true;
  attribute syn_preserve of out_reg : signal is true;
  
  attribute syn_keep : boolean;
  attribute syn_keep of mult_gated : signal is true;
  attribute syn_keep of out_reg : signal is true;
  attribute syn_keep of OUTP : signal is true;

  attribute syn_maxfan : integer;
--   attribute syn_maxfan of out_reg : signal is 1;  
  attribute syn_maxfan of mult_gated : signal is 1;  
  attribute syn_maxfan of current_multiplicity : signal is 1;  
begin
  reg_inp <= INP;
  mult_gated(INP'range) <= reg_INP and CONF.enable(INP'range) when rising_edge(CLK);


  PROC_MULT : process 
    variable m,n : integer range 0 to 8;
  begin
    wait until rising_edge(CLK);
    for j in 0 to 4 loop
      m := 0; n := 0;
      for i in 16*j+0 to 16*j+7 loop
        if mult_gated(i) = '1' then
          m := m + 1;
        end if;  
      end loop;
      for i in 16*j+8 to 16*j+15 loop
        if mult_gated(i) = '1' then
          n := n + 1;
        end if;  
      end loop;
      current_multiplicity(j) <= to_unsigned(m+n,5);
    end loop;
    -- + (current_multiplicity(4) + current_multiplicity(5))
    result <= (current_multiplicity(0) + current_multiplicity(1)) + (current_multiplicity(2) + current_multiplicity(3)) ;
    if result + current_multiplicity(4) >= CONF.multiplicity and CONF.multiplicity > 0 then
      out_reg <= '1';
    else
      out_reg <= '0';
    end if;
  end process;
  
--   PROC_MULT : process 
--     variable m,n : integer range 0 to 20;
--   begin
--     wait until rising_edge(CLK);
--     
--     for j in 0 to 3 loop
--       m := 0; n := 0;
--       for i in 20*j+0 to 20*j+9 loop
--         if mult_gated(i) = '1' then
--           m := m + 1;
--         end if;  
--       end loop;
--       for i in 20*j+10 to 20*j+19 loop
--         if mult_gated(i) = '1' then
--           n := n + 1;
--         end if;  
--       end loop;
--       current_multiplicity(j) <= to_unsigned(m+n,5);
--     end loop;
--     -- + (current_multiplicity(4) + current_multiplicity(5))
--     result <= (current_multiplicity(0) + current_multiplicity(1)) + (current_multiplicity(2) + current_multiplicity(3));
--     if result >= CONF.multiplicity and CONF.multiplicity > 0 then
--       out_reg <= '1';
--     else
--       out_reg <= '0';
--     end if;
--   end process;  
  
OUTP <= out_reg; --'1' when result >= CONF.multiplicity and CONF.multiplicity > 0 else '0' ;

end architecture;
