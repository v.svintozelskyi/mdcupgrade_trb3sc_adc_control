library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;
  use work.config.all;
  use work.triggerbox_pkg.all;

entity triggerbox is
  generic (
    NUM_PHYS_INPUTS : integer range 32 to 128 := 96;
    NUM_INPUTS  : integer range 16 to 128 := 48;
    NUM_VIRTUALIN : integer range 16 to 32 := 32;
    NUM_OUTPUTS : integer range 1 to 16 := 8;
    NUM_GROUPS  : integer range 16 to 32 := 16;
    NUM_COINC   : integer range 16 to 32 := 16;
    NUM_MULT    : integer range 0 to 32 := 8
    );
  port (
    CLK_SYS     : in  std_logic;
    CLK_FULL    : in  std_logic;
    RESET       : in  std_logic;
    
    INP         : in  std_logic_vector(NUM_PHYS_INPUTS-1 downto 0);
    OUTP        : out std_logic_vector(NUM_OUTPUTS-1 downto 0);
    MONITOR_OUT : out std_logic_vector(15 downto 0);

    BUS_RX  : in  CTRLBUS_RX;
    BUS_TX  : out CTRLBUS_TX;
    BUSSCALER_RX  : in  CTRLBUS_RX;
    BUSSCALER_TX  : out CTRLBUS_TX;
    TIMER : in TIMERS;
    
    BUSRDO_RX : in READOUT_RX;
    BUSRDO_TX : out READOUT_TX
    );
end entity;


architecture arch of triggerbox is
constant NUM_SCALER : integer := NUM_OUTPUTS + NUM_MULT + NUM_COINC + NUM_GROUPS + NUM_VIRTUALIN + NUM_INPUTS + NUM_PHYS_INPUTS;
signal reg_inp : std_logic_vector(INP'range);
signal selected_inp, processed_inp, reg_processed_inp, temp_processed_inp  : std_logic_vector(NUM_INPUTS+NUM_VIRTUALIN-1 downto 0);
signal muxed_inp : std_logic_vector(NUM_VIRTUALIN-1 downto 0);
signal list_of_inp : std_logic_vector(255 downto 0);
signal reset_processor : std_logic;
signal groups, reg_groups : std_logic_vector(NUM_GROUPS-1 downto 0);
signal coincs, reg_coincs : std_logic_vector(NUM_COINC-1 downto 0);
signal multis, reg_multis : std_logic_vector(NUM_MULT-1 downto 0);
signal reg_out, out1_i, out2_i, out_i, long_reg_out_1, long_reg_out_2 : std_logic_vector(OUTP'range);

signal list_of_scaler : std_logic_vector(NUM_SCALER-1 downto 0);

type CONF_input_processor_arr is array(0 to NUM_INPUTS+NUM_VIRTUALIN-1) of CONF_input_processor_t;
signal CONF_input_processor : CONF_input_processor_arr;

type CONF_input_mux_arr is array(0 to NUM_VIRTUALIN-1) of std_logic_vector(7 downto 0);
signal CONF_input_mux : CONF_input_mux_arr;

type CONF_coincidence_arr is array(0 to NUM_COINC-1) of CONF_coincidence_t;
signal CONF_coincidence : CONF_coincidence_arr;

type CONF_multiplicity_arr is array(0 to NUM_MULT-1) of CONF_multiplicity_t;
signal CONF_multiplicity : CONF_multiplicity_arr;

type CONF_outputs_arr is array(0 to 1) of std_logic_vector(255 downto 0);
type CONF_outputs_arr2 is array(0 to NUM_OUTPUTS-1) of CONF_outputs_arr;
signal CONF_outputs : CONF_outputs_arr2;

type CONF_groups_arr is array(0 to NUM_GROUPS-1) of std_logic_vector(127 downto 0);
signal CONF_groups : CONF_groups_arr;

type CONF_monitor_arr is array(0 to 15) of integer range 0 to 511;
signal CONF_monitor : CONF_monitor_arr;

type config_mem_t is array(0 to 1023) of std_logic_vector(31 downto 0);
signal config_mem : config_mem_t;

signal CONF_output_strecher : std_logic_vector(NUM_OUTPUTS-1 downto 0);

signal config_mem_out : std_logic_vector(31 downto 0);
signal read_mem  : std_logic;
signal coinc_inputs : std_logic_vector(255 downto 0) := (others => '0');
signal list_of_outp : std_logic_vector(255 downto 0) := (others => '0');
signal list_of_monitor, temp_list_of_monitor : std_logic_vector(511 downto 0) := (others => '0');
signal mult_inputs  : std_logic_vector(79 downto 0) := (others => '0');

signal timer_us, timer_ms : integer range 0 to 1000;
signal tick_us, tick_ms : std_logic;

  attribute syn_preserve : boolean;
  attribute syn_preserve of tick_us : signal is true;
  attribute syn_preserve of tick_ms : signal is true;
  attribute syn_preserve of reg_processed_inp : signal is true;
  attribute syn_preserve of temp_processed_inp : signal is true;
  attribute syn_preserve of reg_groups : signal is true;
  attribute syn_preserve of reg_multis : signal is true;
  attribute syn_preserve of reg_coincs : signal is true;
  attribute syn_preserve of list_of_monitor : signal is true;
  attribute syn_preserve of list_of_scaler : signal is true;

  attribute syn_keep : boolean;
  attribute syn_keep of tick_us : signal is true;
  attribute syn_keep of tick_ms : signal is true;
--   attribute syn_keep of reg_processed_inp : signal is true;
--   attribute syn_keep of temp_processed_inp : signal is true;
--   attribute syn_keep of reg_groups : signal is true;
--   attribute syn_keep of reg_multis : signal is true;
--   attribute syn_keep of reg_coincs : signal is true;
--   attribute syn_keep of list_of_monitor : signal is true;
--   attribute syn_keep of list_of_scaler : signal is true;

  attribute syn_maxfan : integer;
  attribute syn_maxfan of tick_us : signal is 10000;
  attribute syn_maxfan of tick_ms : signal is 10000;
  attribute syn_maxfan of reset_processor : signal is 10000;
  
--   attribute syn_maxfan of list_of_inp       : signal is 16;
  attribute syn_maxfan of reg_processed_inp : signal is 1;
  attribute syn_maxfan of temp_processed_inp : signal is 1;
  attribute syn_maxfan of processed_inp     : signal is 1;
  attribute syn_maxfan of reg_groups        : signal is 1;
  attribute syn_maxfan of reg_multis        : signal is 1;
  attribute syn_maxfan of reg_coincs        : signal is 1;
  attribute syn_maxfan of list_of_monitor   : signal is 1;
  attribute syn_maxfan of temp_list_of_monitor : signal is 1;
 
 
begin

TIMER_PROC: process begin
  wait until rising_edge(CLK_FULL);
  tick_us <= '0';
  tick_ms <= '0';
  
  timer_us <= timer_us + 1;
  if timer_us >= 199 then
    timer_us <= 0;
    tick_us <= '1';
  end if;  
  if tick_us = '1' then
    timer_ms <= timer_ms + 1;
  end if;
  if timer_ms >= 1000 then
    timer_ms <= 0;
    tick_ms <= '1';
  end if;  
  
end process;

--------------------------------
-- Register input
--------------------------------
  PROC_REG : process begin
    wait until rising_edge(CLK_FULL);
    reg_inp <= INP;
  end process;
  
  --TODO: add async stretcher option



--------------------------------
-- Select Inputs
--------------------------------
list_of_inp(NUM_PHYS_INPUTS-1 downto 0) <= inp;
gen_input_muxes : for i in 0 to NUM_VIRTUALIN-1 generate
  muxed_inp(i) <= list_of_inp(to_integer(unsigned(CONF_input_mux(i))));-- when rising_edge(CLK_FULL);
end generate;

selected_inp <= muxed_inp & inp(NUM_INPUTS-1 downto 0);


--------------------------------
-- Input Stage
--------------------------------
gen_input_processor : for i in 0 to NUM_INPUTS+NUM_VIRTUALIN-1 generate
  THE_INP_PROC : entity work.input_processor 
    port map(
      CLK  => CLK_FULL,
      RESET => reset_processor,
      INP  => selected_inp(i),
      OUTP => processed_inp(i),
      TICK_MS => tick_ms,
      TICK_US => tick_us,
      CONF => CONF_input_processor(i)
    );
end generate;

temp_processed_inp <= processed_inp when rising_edge(CLK_FULL);
reg_processed_inp <= temp_processed_inp when rising_edge(CLK_FULL);


--------------------------------
-- Or Groups
--------------------------------
gen_groups : for i in 0 to NUM_GROUPS-1 generate
  THE_GROUPS : entity work.groups
    generic map (
      NUM_INPUTS => NUM_INPUTS+NUM_VIRTUALIN
      )
    port map(
      CLK      => CLK_FULL,
      INP      => reg_processed_inp,
      OUTP     => groups(i),
      CONF     => CONF_groups(i)(reg_processed_inp'range)
      );
end generate;
reg_groups <= groups when rising_edge(CLK_FULL);

--------------------------------
-- Coincidences
--------------------------------

coinc_inputs(NUM_MULT + NUM_GROUPS + NUM_INPUTS + NUM_VIRTUALIN -1 downto 0) <= reg_multis & reg_groups & reg_processed_inp;
gen_coincs : for i in 0 to NUM_COINC-1 generate
  THE_COINC : entity work.coincidence
    generic map (
      NUM_INPUTS => NUM_MULT + NUM_GROUPS + NUM_INPUTS + NUM_VIRTUALIN
      )
    port map(
      CLK      => CLK_FULL,
      INP      => coinc_inputs(NUM_MULT + NUM_GROUPS + NUM_INPUTS + NUM_VIRTUALIN-1 downto 0),
      OUTP     => coincs(i),
      CONF     => CONF_coincidence(i)
      );
end generate;
reg_coincs <= coincs when rising_edge(CLK_FULL);

--------------------------------
-- Multiplicities
--------------------------------

mult_inputs <= reg_processed_inp(63 downto 48) & reg_processed_inp(31 downto 0) & reg_coincs(15 downto 0) & reg_groups(15 downto 0);
gen_multis : for i in 0 to NUM_MULT-1 generate
  THE_MULTIPLICITY : entity work.multiplicity
    generic map(
      NUM_INPUTS => 80
      )
    port map(
      CLK  => CLK_FULL,
      INP  => mult_inputs,
      CONF => CONF_multiplicity(i),
      OUTP => multis(i)
      );
end generate;
reg_multis <= multis when rising_edge(CLK_FULL);

--------------------------------
-- Outputs
--------------------------------

list_of_outp(NUM_MULT + NUM_COINC + NUM_VIRTUALIN + NUM_INPUTS-1 downto 0) <= reg_multis & reg_coincs &  reg_processed_inp ;--when rising_edge(CLK_FULL);
gen_outputs : for i in 0 to NUM_OUTPUTS-1 generate
  out1_i(i) <= or (list_of_outp and CONF_outputs(i)(0)) when rising_edge(CLK_FULL);
  out2_i(i) <= or (list_of_outp and CONF_outputs(i)(1)) when rising_edge(CLK_FULL);
end generate;

reg_out <= out_i or ((long_reg_out_1 or long_reg_out_2) and CONF_output_strecher) when  rising_edge(CLK_FULL);
out_i <= out1_i and out2_i;
long_reg_out_1 <= out_i          when  rising_edge(CLK_FULL);
long_reg_out_2 <= long_reg_out_1 when  rising_edge(CLK_FULL);
OUTP <= reg_out;

--------------------------------
-- Registers
--------------------------------

proc_REGS : process 
  variable slice : integer range 0 to 127;
begin
  wait until rising_edge(CLK_SYS);
  BUS_TX.ack <= read_mem;
  BUS_TX.data <= config_mem_out;
  BUS_TX.nack <= '0';
  BUS_TX.unknown <= '0';
  read_mem <= '0';
  config_mem_out <= config_mem(to_integer(unsigned(BUS_RX.addr(9 downto 0))));
  reset_processor <= '0';
  
  if BUS_RX.write = '1' then
    if BUS_RX.addr(11 downto 10) = "00" then
      config_mem(to_integer(unsigned(BUS_RX.addr(9 downto 0)))) <= BUS_RX.data;
    end if;
    if BUS_RX.addr(11 downto 7) = "00000" then
      slice := to_integer(unsigned(BUS_RX.addr(6 downto 0)));
      if slice < NUM_INPUTS + NUM_VIRTUALIN then
        BUS_TX.ack <= '1';
        reset_processor <= '1';
        CONF_input_processor(slice).async  <= BUS_RX.data(0);
        CONF_input_processor(slice).enable <= BUS_RX.data(1);
        CONF_input_processor(slice).invert <= BUS_RX.data(2);
        CONF_input_processor(slice).edge_detect <= BUS_RX.data(3);
        CONF_input_processor(slice).stretch_unit <= BUS_RX.data(5 downto 4);
        CONF_input_processor(slice).stretch_value <= BUS_RX.data(17 downto 6);
        CONF_input_processor(slice).delay <= BUS_RX.data(27 downto 18);
        CONF_input_processor(slice).downscale <= BUS_RX.data(31 downto 28);
      else
        BUS_TX.unknown <= '1';
      end if;  

    elsif BUS_RX.addr(11 downto 7) = "00001" then
      slice := to_integer(unsigned(BUS_RX.addr(6 downto 2)));
      if slice < NUM_GROUPS then
        BUS_TX.ack <= '1';
        case BUS_RX.addr(1 downto 0) is
          when "00" => CONF_groups(slice)(31  downto  0) <= BUS_RX.data(31 downto 0);
          when "01" => CONF_groups(slice)(63  downto 32) <= BUS_RX.data(31 downto 0);
          when "10" => CONF_groups(slice)(95  downto 64) <= BUS_RX.data(31 downto 0);
          when "11" => CONF_groups(slice)(127 downto 96) <= BUS_RX.data(31 downto 0);
          when others => null;
        end case;          
      else
        BUS_TX.unknown <= '1';
      end if;
      
    elsif BUS_RX.addr(11 downto 5) = "0001000" then
      slice := to_integer(unsigned(BUS_RX.addr(4 downto 0)));
      if slice < NUM_VIRTUALIN then
        BUS_TX.ack <= '1';
        CONF_input_mux(slice) <= BUS_RX.data(7 downto 0);
      else
        BUS_TX.unknown <= '1';
      end if;
    
    elsif BUS_RX.addr(11 downto 5) = "0001001" then
      slice := to_integer(unsigned(BUS_RX.addr(4 downto 0)));
      if slice < NUM_COINC then
        BUS_TX.ack <= '1';
        CONF_coincidence(slice).mux_input1    <= to_integer(unsigned(BUS_RX.data(7 downto 0)));
        CONF_coincidence(slice).mux_input2    <= to_integer(unsigned(BUS_RX.data(15 downto 8)));
        CONF_coincidence(slice).mux_input3    <= to_integer(unsigned(BUS_RX.data(23 downto 16)));
        CONF_coincidence(slice).invert        <= BUS_RX.data(26 downto 24);
        CONF_coincidence(slice).enable        <= BUS_RX.data(29 downto 27);
        CONF_coincidence(slice).module_enable <= BUS_RX.data(30);
      else
        BUS_TX.unknown <= '1';
      end if;
    
    elsif BUS_RX.addr(11 downto 5) = "0001010" then
      slice := to_integer(unsigned(BUS_RX.addr(4 downto 0)));
      if slice < NUM_MULT then
        BUS_TX.ack <= '1';
        CONF_multiplicity(slice).multiplicity  <= unsigned(BUS_RX.data(4 downto 0));        
      else
        BUS_TX.unknown <= '1';
      end if;

    elsif BUS_RX.addr(11 downto 4) = "00010110" then
        BUS_TX.ack <= '1';
        slice := to_integer(unsigned(BUS_RX.addr(3 downto 0)));
        CONF_monitor(slice)  <= to_integer(unsigned(BUS_RX.data(8 downto 0)));
      
    elsif BUS_RX.addr(11 downto 7) = "00011" then
      slice := to_integer(unsigned(BUS_RX.addr(6 downto 2)));
      if slice < NUM_MULT then
        BUS_TX.ack <= '1';
        if(BUS_RX.addr(1 downto 0) = "00") then
          CONF_multiplicity(slice).enable(31 downto 0)  <= BUS_RX.data;
        elsif(BUS_RX.addr(1 downto 0) = "01") then
          CONF_multiplicity(slice).enable(63 downto 32)  <= BUS_RX.data;
        elsif(BUS_RX.addr(1 downto 0) = "11") then
          CONF_multiplicity(slice).enable(95 downto 64)  <= BUS_RX.data;
        end if;
      else
        BUS_TX.unknown <= '1';
      end if;
      
    elsif BUS_RX.addr(11 downto 0) = "000111000000" then
        BUS_TX.ack <= '1';
        CONF_output_strecher  <= BUS_RX.data(NUM_OUTPUTS-1 downto 0);
        
    elsif BUS_RX.addr(11 downto 7) = "00100" then
      slice := to_integer(unsigned(BUS_RX.addr(6 downto 3)));
      if slice < NUM_OUTPUTS then
        BUS_TX.ack <= '1';
        case BUS_RX.addr(2 downto 0) is
          when "000" => CONF_outputs(slice)(0)( 31 downto   0)  <= BUS_RX.data;
          when "001" => CONF_outputs(slice)(0)( 63 downto  32)  <= BUS_RX.data;
          when "010" => CONF_outputs(slice)(0)( 95 downto  64)  <= BUS_RX.data;
          when "011" => CONF_outputs(slice)(0)(127 downto  96)  <= BUS_RX.data;
          when "100" => CONF_outputs(slice)(0)(159 downto 128)  <= BUS_RX.data;
          when "101" => CONF_outputs(slice)(0)(191 downto 160)  <= BUS_RX.data;
          when "110" => CONF_outputs(slice)(0)(223 downto 192)  <= BUS_RX.data;
          when "111" => CONF_outputs(slice)(0)(255 downto 224)  <= BUS_RX.data;
          when others => null;
        end case;
      end if;  
   elsif BUS_RX.addr(11 downto 7) = "00110" then
      slice := to_integer(unsigned(BUS_RX.addr(6 downto 3)));
      if slice < NUM_OUTPUTS then
        BUS_TX.ack <= '1';
        case BUS_RX.addr(2 downto 0) is
          when "000" => CONF_outputs(slice)(1)( 31 downto   0)  <= BUS_RX.data;
          when "001" => CONF_outputs(slice)(1)( 63 downto  32)  <= BUS_RX.data;
          when "010" => CONF_outputs(slice)(1)( 95 downto  64)  <= BUS_RX.data;
          when "011" => CONF_outputs(slice)(1)(127 downto  96)  <= BUS_RX.data;
          when "100" => CONF_outputs(slice)(1)(159 downto 128)  <= BUS_RX.data;
          when "101" => CONF_outputs(slice)(1)(191 downto 160)  <= BUS_RX.data;
          when "110" => CONF_outputs(slice)(1)(223 downto 192)  <= BUS_RX.data;
          when "111" => CONF_outputs(slice)(1)(255 downto 224)  <= BUS_RX.data;
        end case;  
      else
        BUS_TX.unknown <= '1';
      end if;
      
    else
      BUS_TX.unknown <= '1';
    end if;
    
  elsif BUS_RX.read = '1' then
    if   (BUS_RX.addr(11 downto 7) = "00000"   and to_integer(unsigned(BUS_RX.addr(6 downto 0))) < NUM_INPUTS + NUM_VIRTUALIN)
      or (BUS_RX.addr(11 downto 7) = "00001"   and to_integer(unsigned(BUS_RX.addr(6 downto 2))) < NUM_GROUPS)
      or (BUS_RX.addr(11 downto 5) = "0001000" and to_integer(unsigned(BUS_RX.addr(4 downto 0))) < NUM_VIRTUALIN)
      or (BUS_RX.addr(11 downto 5) = "0001001" and to_integer(unsigned(BUS_RX.addr(4 downto 0))) < NUM_COINC)
      or (BUS_RX.addr(11 downto 5) = "0001010" and to_integer(unsigned(BUS_RX.addr(4 downto 0))) < NUM_MULT)
      or (BUS_RX.addr(11 downto 4) = "00010110"and to_integer(unsigned(BUS_RX.addr(3 downto 0))) < 16)
      or (BUS_RX.addr(11 downto 7) = "00011"   and to_integer(unsigned(BUS_RX.addr(6 downto 2))) < NUM_MULT)
      or (BUS_RX.addr(11 downto 7) = "00100"   and to_integer(unsigned(BUS_RX.addr(6 downto 3))) < NUM_OUTPUTS)
      or (BUS_RX.addr(11 downto 7) = "00110"   and to_integer(unsigned(BUS_RX.addr(6 downto 3))) < NUM_OUTPUTS)
      or (BUS_RX.addr(11 downto 0) = "000111000000")
      then
      read_mem <= '1';
      
    elsif BUS_RX.addr(11 downto 0) = x"F00" then
      BUS_TX.data <=  x"00"
                      & std_logic_vector(to_unsigned(NUM_PHYS_INPUTS,8)) 
                      & std_logic_vector(to_unsigned(NUM_VIRTUALIN,8)) 
                      & std_logic_vector(to_unsigned(NUM_INPUTS,8));
      BUS_TX.ack <= '1';
      
    elsif BUS_RX.addr(11 downto 0) = x"F01" then
      BUS_TX.data <=  x"00"
                      & std_logic_vector(to_unsigned(NUM_MULT,8)) 
                      & std_logic_vector(to_unsigned(NUM_COINC,8)) 
                      & std_logic_vector(to_unsigned(NUM_GROUPS,8));
      BUS_TX.ack <= '1';
    elsif BUS_RX.addr(11 downto 0) = x"F02" then
      BUS_TX.data <=    std_logic_vector(to_unsigned(NUM_SCALER,16)) 
                      & std_logic_vector(to_unsigned(16,8)) --NUM_MONITORS
                      & std_logic_vector(to_unsigned(NUM_OUTPUTS,8));
      BUS_TX.ack <= '1';      
    else
      BUS_TX.unknown <= '1';
    end if;
  end if;
end process;

--------------------------------
-- Scaler
--------------------------------
list_of_scaler <= reg_out & reg_multis & reg_coincs & reg_groups & reg_processed_inp & reg_inp  when rising_edge(CLK_FULL);
THE_SCALER : entity work.scalers
  generic map(
    NUM_INPUTS => NUM_SCALER
    )
  port map(
    CLK_SYS  => CLK_SYS,
    CLK_FULL => CLK_FULL,
    RESET    => RESET,
    INP      => list_of_scaler,
    BUS_RX   => BUSSCALER_RX,
    BUS_TX   => BUSSCALER_TX,
    TIMER    => TIMER,
    
    RDO_RX   => BUSRDO_RX,
    RDO_TX   => BUSRDO_TX
    );

    
--------------------------------
-- Monitor  
--------------------------------
list_of_monitor(NUM_SCALER-1 downto 0) <= reg_out & reg_multis & reg_coincs & reg_groups & reg_processed_inp & reg_inp when rising_edge(CLK_FULL);
-- list_of_monitor <= temp_list_of_monitor when rising_edge(CLK_FULL);
gen_monitors : for i in MONITOR_OUT'range generate
  THE_MONITOR : entity work.monitor_output
    generic map(
      NUM_INPUTS => NUM_SCALER
      )
    port map(
      CLK => CLK_FULL,
      INP => list_of_monitor(NUM_SCALER-1 downto 0),
      OUTP => MONITOR_OUT(i),
      CONF => CONF_monitor(i)
      );
end generate;    
  

--------------------------------
-- Readout
--------------------------------


end architecture;
