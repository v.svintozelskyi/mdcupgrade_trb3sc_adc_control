library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;
  use work.config.all;
  use work.triggerbox_pkg.all;


entity input_processor is
  port (
    CLK      : in  std_logic;
    RESET    : in  std_logic;
    INP      : in  std_logic;  
    OUTP     : out std_logic;
    TICK_MS  : in  std_logic;
    TICK_US  : in  std_logic;
    CONF     : in  CONF_input_processor_t
    );
end entity;


architecture arch of input_processor is
attribute syn_hier : string;
--attribute syn_hier of arch : architecture is "fixed";

attribute HGROUP: string;
attribute BBOX: string;
attribute HGROUP of arch : architecture is "input_proc";
attribute BBOX   of arch: architecture is "4,16";

signal reg_inp, gated_inp, edged_inp, stretched_inp, reg_stretched_inp, delayed_inp, downscaled_inp : std_logic;
signal temp_edged_inp, temp_downscaled_inp, temp_stretched_inp, temp_delayed_inp : std_logic;
signal reg_gated_inp,reg2_gated_inp : std_logic;

signal reg_tick_us, reg_tick_ms : std_logic;
signal timer : unsigned(11 downto 0);
signal counter : unsigned(15 downto 0);
signal reg_outp : std_logic;

  attribute syn_preserve : boolean;
  attribute syn_preserve of reg_tick_us : signal is true;
  attribute syn_preserve of reg_tick_ms : signal is true;
  attribute syn_preserve of reg_inp : signal is true;

  attribute syn_keep : boolean;
  attribute syn_keep of reg_tick_us : signal is true;
  attribute syn_keep of reg_tick_ms : signal is true;
  attribute syn_keep of reg_inp : signal is true;

  attribute syn_maxfan : integer;
--   attribute syn_maxfan of reg_outp : signal is 10;  
  
begin

  reg_tick_us <= TICK_US when rising_edge(CLK);
  reg_tick_ms <= TICK_MS when rising_edge(CLK);

  PROC_REG : process begin
    wait until rising_edge(CLK);
    reg_inp <= INP;
  end process;
  
  --TODO: add async stretcher option
  
--------------------------------
-- Gate / Invert
--------------------------------
  
  gated_inp <= reg_INP when CONF.enable = '1' and CONF.invert = '0' else
               not reg_INP when CONF.enable = '1' and CONF.invert = '1' else
               '0';
  
  reg_gated_inp  <= gated_inp      when rising_edge(CLK);
--   reg2_gated_inp <= reg_gated_inp  when rising_edge(CLK);
  
--------------------------------
-- Edge Detect
--------------------------------
  PROC_EDGE : process(CLK)
    begin
      if rising_edge(CLK) then
        temp_edged_inp <= not reg_gated_inp and gated_inp;
      end if;
    end process;  

  edged_inp <= gated_inp when CONF.edge_detect = '0' else --and CONF.downscale = x"0"
               temp_edged_inp;
      

-- --------------------------------
-- -- Downscale
-- --------------------------------    
--   PROC_DOWNSCALE : process(CLK)
--     variable countmax : unsigned(15 downto 0);
--     begin
--       if rising_edge(CLK) then
--         temp_downscaled_inp <= '0';
--         if RESET = '1' then
--           counter <= (others => '0');
--         else  
--           if reg_gated_inp = '0' and gated_inp = '1' then
--             if counter > 0 then
--               counter <= counter - 1;
--             else  
--               countmax := (others => '0');
--               countmax(to_integer(unsigned(CONF.downscale))) := '1';
--               counter <= countmax-1;
--               temp_downscaled_inp <= '1';
--             end if;
--           end if;
--         end if;  
--       end if;
--     end process;
--     
--   downscaled_inp <= edged_inp when CONF.downscale = x"0" else
--                     temp_downscaled_inp;
  
--------------------------------
-- Stretch
--------------------------------
  PROC_STRETCH : process(CLK)
    begin
      if rising_edge(CLK) then
        if RESET = '1' then
          timer <= (others => '0');
          temp_stretched_inp <= '0';
        elsif (edged_inp = '1') then
          timer <= unsigned(CONF.stretch_value);
          temp_stretched_inp <= '1';
        elsif timer = 0 then
          temp_stretched_inp <= '0';
        elsif     CONF.stretch_unit = "01" 
              or (CONF.stretch_unit = "10" and reg_tick_us = '1')
              or (CONF.stretch_unit = "11" and reg_tick_ms = '1') then
          timer <= timer - 1;
        end if;  
      end if;
    end process;

  stretched_inp <= edged_inp when CONF.stretch_unit = "00" else
                   temp_stretched_inp;    
    
    
    
--------------------------------
-- Delay
--------------------------------
  reg_stretched_inp <= stretched_inp when rising_edge(CLK);

  THE_SHIFT: entity work.delay_shift_reg                                 
    port map(
      Din(0)  =>    reg_stretched_inp,
      Addr    =>    CONF.delay(7 downto 0),
      Clock   =>    CLK,
      ClockEn =>    '1',
      Reset   =>    RESET,
      Q(0)    =>    temp_delayed_inp    
      );
        
  delayed_inp <= stretched_inp when CONF.delay(7 downto 0) = x"00" else
                 temp_delayed_inp;



--------------------------------
-- Output
--------------------------------

  reg_outp <= delayed_inp when rising_edge(CLK);
  OUTP <= reg_outp;
  
end architecture;
