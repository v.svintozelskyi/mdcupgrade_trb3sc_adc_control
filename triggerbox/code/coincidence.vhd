library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;
  use work.config.all;
  use work.triggerbox_pkg.all;


entity coincidence is
  generic (
    NUM_INPUTS : integer range 1 to 256 := 32
    );
  port (
    CLK      : in  std_logic;
    INP      : in  std_logic_vector(NUM_INPUTS-1 downto 0);
    OUTP     : out std_logic;
    CONF     : in  CONF_coincidence_t
    );
end entity;


architecture arch of coincidence is

attribute syn_hier : string;
attribute syn_hier of arch : architecture is "fixed";

attribute HGROUP: string;
attribute HGROUP of arch : architecture is "coinc_proc";
attribute BBOX: string;
attribute BBOX   of arch: architecture is "8,8";

signal reg_inp : std_logic_vector(INP'range);
signal sel1,sel2,sel3 : std_logic;
signal reg_out : std_logic;

  attribute syn_preserve : boolean;
  attribute syn_preserve of reg_inp : signal is true;
  attribute syn_preserve of reg_out : signal is true;
  
  attribute syn_keep : boolean;
  attribute syn_keep of reg_inp : signal is true;
  attribute syn_keep of reg_out : signal is true;

-- (1 or 2) and 3
begin
reg_inp <= INP when rising_edge(CLK);

sel1 <= reg_inp(CONF.mux_input1);
sel2 <= reg_inp(CONF.mux_input2);
sel3 <= reg_inp(CONF.mux_input3);

reg_out <=     (((sel1 xor CONF.invert(0)) and CONF.enable(0))
          or ((sel2 xor CONF.invert(1)) and CONF.enable(1)))
          
          and ((sel3 xor CONF.invert(2)) or not CONF.enable(2))
          and CONF.module_enable  
          when rising_edge(CLK);

OUTP <= reg_out;          
          
end architecture;
