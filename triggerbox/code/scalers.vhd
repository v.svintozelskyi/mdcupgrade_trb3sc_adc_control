library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;
  use work.config.all;
  use work.triggerbox_pkg.all;


entity scalers is
  generic (
    NUM_INPUTS : integer range 1 to 512 := 32
    );
  port (
    CLK_SYS  : in  std_logic;
    CLK_FULL : in  std_logic;
    RESET    : in  std_logic;
    INP      : in  std_logic_vector(NUM_INPUTS-1 downto 0);
    
    BUS_RX   : in  CTRLBUS_RX;
    BUS_TX   : out CTRLBUS_TX;
    TIMER    : in  TIMERS;
    RDO_RX   : in  READOUT_RX;
    RDO_TX   : out READOUT_TX
    );
end entity;


architecture arch of scalers is

attribute syn_hier : string;
attribute syn_hier of arch : architecture is "fixed";

attribute HGROUP: string;
attribute HGROUP of arch : architecture is "scalers";
attribute BBOX: string;
attribute HGROUP of arch : architecture is "input_proc";
attribute BBOX   of arch: architecture is "60,40";


signal reg_inp, reg2_inp, reg3_inp, reg4_inp, cnt_en : std_logic_vector(INP'range);

  attribute syn_preserve : boolean;
  attribute syn_preserve of reg_inp : signal is true;
  
  attribute syn_keep : boolean;
  attribute syn_keep of reg_inp : signal is true;

type cnt_arr is array (0 to NUM_INPUTS-1) of unsigned(27 downto 0);  
signal cnt : cnt_arr;  

signal reset_cnt : std_logic := '0';
signal send_ack, reg_send_ack, reg2_send_ack, reg3_send_ack : std_logic := '0'; 
signal muxreg, rdo_muxreg : std_logic_vector(31 downto 0);

  type rdo_state_t is (IDLE, ADDTIME, WRITEFIRST, WRITE, WAIT1, WAIT2, WAIT3, CALC, FINISH, BUSYEND);
  signal state : rdo_state_t;
  signal addr,ram_addr : integer range 0 to 255;

  type ram_arr is array (0 to 255) of std_logic_vector(31 downto 0);  
  signal ram : ram_arr;  
  
  signal ram_din, ram_dout, ram_dout_i : std_logic_vector(31 downto 0);
  signal result : std_logic_vector(31 downto 0);
  signal ram_write: std_logic;
  
  
  
begin
reg_inp <= INP when rising_edge(CLK_FULL);
reg2_inp <= reg_inp when rising_edge(CLK_FULL);
reg3_inp <= reg2_inp when rising_edge(CLK_FULL);
reg4_inp <= reg3_inp when rising_edge(CLK_SYS);

--------------------------------
-- Scaler
--------------------------------
gen_scalers : for i in 0 to NUM_INPUTS-1 generate
  process begin
    wait until rising_edge(CLK_FULL);
    cnt_en(i) <= reg2_inp(i) and not reg3_inp(i);
    if cnt_en(i) = '1' then
      cnt(i) <= cnt(i) + 1;
    end if;  
    if RESET = '1' then
      cnt(i) <= (others => '0');
    end if;
  end process;
end generate;


--------------------------------
-- CTRL readout
--------------------------------
muxreg(27 downto 0) <= cnt(to_integer(unsigned(BUS_RX.addr(8 downto 0)))) when rising_edge(CLK_SYS);
muxreg(31) <= reg4_inp(to_integer(unsigned(BUS_RX.addr(8 downto 0))));

THE_REGS : process 
  variable slice : integer range 0 to 511;
begin
  wait until rising_edge(CLK_SYS);
  BUS_TX.ack <= reg2_send_ack;
  BUS_TX.nack <= '0';
  BUS_TX.unknown <= '0';  
  BUS_TX.data <= muxreg;
  
  send_ack <= '0';
  reg_send_ack  <= send_ack;
  reg2_send_ack <= reg_send_ack;
  reg3_send_ack <= reg2_send_ack;
  
  if BUS_RX.read = '1' then
    if BUS_RX.addr(9) = '0' then
      slice := to_integer(unsigned(BUS_RX.addr(8 downto 0)));
      if slice < NUM_INPUTS then
        send_ack <= '1';
      else
        BUS_TX.unknown <= '1';      
      end if;
    end if;
  end if;
end process;  

--------------------------------
-- Scaler memory
--------------------------------

  THE_MEM : process
    begin
      wait until rising_edge(CLK_SYS);
      if ram_write = '1' then
        ram(ram_addr) <= ram_din;
      end if;
      ram_dout_i <= ram(ram_addr);
    end process;
    
ram_din  <= rdo_muxreg;
ram_dout <= ram_dout_i when rising_edge(CLK_SYS);
ram_addr <= addr;




--------------------------------
-- Trigger Handling
--------------------------------
THE_RDO : process begin
  wait until rising_edge(CLK_SYS);
  RDO_TX.busy_release  <= '0';
  RDO_TX.data_write    <= '0';
  RDO_TX.data_finished <= '0';
  ram_write            <= '0';


  
  case state is
    when IDLE => 
      addr <= 0;
      if RDO_RX.valid_notiming_trg = '1' and RDO_RX.trg_type = x"E"  then
        state <= WRITEFIRST;
      elsif RDO_RX.valid_notiming_trg = '1' or RDO_RX.valid_timing_trg = '1' or RDO_RX.invalid_trg = '1' then
        state <= ADDTIME;
      end if;

    when ADDTIME =>
      RDO_TX.data  <= TIMER.microsecond;
      RDO_TX.data_write <= '1';
      state <= FINISH;      
      
    when WRITEFIRST =>
      RDO_TX.data  <= TIMER.microsecond;
      RDO_TX.data_write <= '1';
      state <= WAIT1;

    when WAIT1 =>
      state <= WAIT2;
      
    when WAIT2 =>
      state <= WAIT3;
      
    when WAIT3 =>
      state <= CALC;        
      rdo_muxreg(27 downto 0) <= cnt(addr);
      
    when CALC =>
      state <= WRITE;
      ram_write <= '1';
      result(27 downto 0) <= std_logic_vector(unsigned(rdo_muxreg(27 downto 0)) - unsigned(ram_dout(27 downto 0)));
      result(31) <= reg4_inp(to_integer(unsigned(BUS_RX.addr(8 downto 0))));
      
    when WRITE =>
      
      RDO_TX.data_write <= '1';
      RDO_TX.data <= result;
      addr <= addr + 1;
      
      if addr = NUM_INPUTS - 1 then
        state <= FINISH;
      else
        state <= WAIT1;

      end if;  

    when FINISH =>
      state <= BUSYEND;
      RDO_TX.data_finished <= '1';
      
    when BUSYEND =>
      state <= IDLE;
      RDO_TX.busy_release <= '1';
  end case;
end process;

end architecture;



