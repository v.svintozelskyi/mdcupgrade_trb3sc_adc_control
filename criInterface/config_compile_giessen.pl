#Familyname  => 'ECP5UM',
#Devicename  => 'LFE5UM-85F',
#Package     => 'CABGA381',
#Speedgrade  => '8',

TOPNAME                      => "trb3sc_criInterface",
lm_license_file_for_synplify => "7788\@fb07pc-u102325",
lm_license_file_for_par      => "7788\@fb07pc-u102325",
lattice_path                 => '/usr/local/diamond/3.11_x64/',
synplify_path                => '/usr/local/diamond/3.11_x64/synpbase',
#synplify_command             => "/opt/lattice/diamond/3.5_x64/bin/lin64/synpwrap -fg -options",
#synplify_command             => "/opt/synplicity/O-2018.09-SP1/bin/synplify_premier_dp",
#synplify_command             => "synpwrap -prj ../trb3sc_criInterface.prj",
synplify_command             => "synpwrap -fg -options",

nodelist_file                => '../nodes_lxhadeb07.txt',


#Include only necessary lpf files
pinout_file                  => 'trb3sc_hub', #name of pin-out file, if not equal TOPNAME
include_TDC                  => 0,
include_GBE                  => 0,

#Report settings
firefox_open                 => 0,
#twr_number_of_errors         => 20,
#no_ltxt2ptxt                 => 1,  #if there is no serdes being used
#make_jed                     => 1,
