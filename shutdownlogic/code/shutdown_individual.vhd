library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all; 

  
entity shutdown_indv is
  port(
  clk_in             : in std_logic;
  signal_in          : in std_logic;
  int_time           : in std_logic_vector (31 downto 0);
  max_count          : in std_logic_vector (31 downto 0);
  current_hits_vctr  : out std_logic_vector (31 downto 0);
  reset_pulse        : out std_logic;
  processed_signal   : out std_logic
  );
end shutdown_indv;




architecture behave of shutdown_indv is

-- declare all signals like pulser 
signal ms_timer : unsigned(15 downto 0):= x"0000";
signal ms_pulse : std_logic;
signal integration_time_int: unsigned(31 downto 0):= x"00000000";
-- signal current_hits_vctr: std_logic_vector (31 downto 0);
signal max_hitcount: unsigned(31 downto 0):= x"00000000";
signal hitcounter : unsigned(31 downto 0):= x"00000000";
signal hitsaver : unsigned(31 downto 0):= x"00000000";
signal hitteller : unsigned(31 downto 0):= x"00000000";
-- signal hitcounter2 : unsigned(31 downto 0):= x"00000000";
signal timecounter : unsigned(31 downto 0):= x"00000000";
signal temp_save1 : std_logic;
signal temp_save2 : std_logic;
signal ext : std_logic;
signal finalout : std_logic;
signal temp_out  : std_logic;
signal reset_pulse_i : std_logic;
signal reset_hitcounter : std_logic;
signal reset_timecounter : std_logic;


begin
  
  integration_time_int <= unsigned(int_time(31 downto 0));
  max_hitcount    <= unsigned(max_count(31 downto 0));
  current_hits_vctr <= std_logic_vector(hitteller);
  reset_pulse <= reset_pulse_i;
  
PROC_SAFETYOFF: process begin                                   -- stops the entity from sending signals before there are values recieved from the bussystem
wait until rising_edge(clk_in);

if max_hitcount = x"00000000" or integration_time_int = x"00000000" then
    processed_signal <= '0';
  else
    processed_signal <=  finalout;
end if;
end process; 
  
  
EDGEDETECT: process is 
begin
wait until rising_edge(clk_in);
  hitsaver <= hitcounter;
  temp_save1 <= signal_in;
  temp_save2 <= temp_save1;
  
if temp_out='1' or ext='1' then
  finalout <= '1';
else 
  finalout <='0';
end if;  
end process;  
  
  
HITCOUNT: process is 
begin
wait until rising_edge(clk_in);
if reset_pulse_i='1' then
  hitcounter <= x"00000000";
elsif temp_save1='1' and temp_save2='0' then
  hitcounter<= hitcounter+1;
else
  hitcounter<=hitcounter+0;
end if;

end process;
  
  
MAXHITS_ALERT: process begin
wait until rising_edge(clk_in);

if hitcounter >= max_hitcount then 
  temp_out <= '1';
else
  temp_out <= '0';
end if;
end process;


POC_OUTPUT: process begin
wait until rising_edge(clk_in);
  if reset_pulse_i='1' then
    hitteller <= hitsaver;
    if temp_out = '1' then
      ext<='1';
    elsif ext='1' then
      ext<='0';
    else
      ext<='0';
    end if;
  end if;
end process;


PROC_MSPUSER: process begin          -- generate a pulse every 0.1 ms
wait until rising_edge (clk_in);
  if ms_timer = x"2710" then           -- eqals 10.000 rising edges
    ms_pulse <= '1';
    ms_timer <= x"0000" ;
  else   
    ms_timer <= ms_timer+1;
    ms_pulse<='0';
  end if;
end process;
  

TIMERESETPULSE: process begin                                -- generates a pulse every time the integration window is closed
wait until rising_edge(clk_in);
  if timecounter >= integration_time_int then 
    reset_pulse_i <= '1';
    timecounter <= x"00000000";
  elsif timecounter < integration_time_int and ms_pulse='1' then      
    timecounter <= timecounter +1;
    reset_pulse_i <= '0';
  else
    reset_pulse_i <= '0';
    
  end if;
end process;
  





end behave;



























