library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;

  
  
entity shutdown_comp is
generic(
      INPUTS  : integer := 24;
      OUTPUTS : integer := 8
      );
  port(
  clk_in             : in std_logic;
  signal_in          : in std_logic_vector(INPUTS-1 downto 0);
  max_number_reg     : in std_logic_vector(31 downto 0);
  disable_reg        : in std_logic_vector(31 downto 0);

  processed_signal   : out std_logic_vector(OUTPUTS-1 downto 0)
  );
end shutdown_comp;


architecture behave of shutdown_comp is

signal max_signals : integer range 0 to 255;
signal sum_signals : integer range 0 to 255;
signal temp_out    : std_logic_vector(OUTPUTS-1 downto 0);




begin
-- max_signals <= to_integer(unsigned(max_number_reg))+1;
max_signals <= to_integer(unsigned(max_number_reg));
sum_signals <= count_ones(signal_in and not disable_reg(INPUTS-1 downto 0));




PROC_SAFETYOFF: process (temp_out)                                    -- stops the entity from sending signals before there are values recieved from the bussystem
begin
if max_signals = 0 then
    processed_signal <= (others => '0');
  else
    processed_signal <= temp_out;
end if;
end process;





PROC_COMPARE: process begin
wait until rising_edge(clk_in);
 if sum_signals >= max_signals then
      temp_out <= (others => '0');
      temp_out(0) <= '1';
      
 else
      temp_out <= (others => '0');
      --processed_signal(0)<='0';
 end if;
 
end process;    
    
end behave;





-- 
--   function count_ones( input:std_logic_vector ) return integer is
--     variable temp:std_logic_vector(input'range);
--     begin
--       temp := (others => '0');
--       for i in input'range loop
-- --        if input(i) = '1' then
--           temp := temp + input(i);
-- --        end if;
--       end loop;
--       return conv_integer(temp);
--       end function count_ones;
