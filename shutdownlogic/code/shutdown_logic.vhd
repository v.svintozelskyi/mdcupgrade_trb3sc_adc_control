 library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all;
  
entity shutdown_logic is
  generic(
    INPUTS  : integer := 24;         
    OUTPUTS : integer := 8
    );
  port(
    CLK      : in std_logic;
    RESET    : in std_logic;
    
    --Slowcontrol
    BUS_RX     : in  CTRLBUS_RX;
    BUS_TX     : out CTRLBUS_TX;
    
    --Inputs and Outputs
    INPUT  : in  std_logic_vector(INPUTS-1 downto 0);
    OUTPUT : out std_logic_vector(OUTPUTS-1 downto 0)
    );
end entity;




architecture arch of shutdown_logic is

-- Registers with values
type Array32bit is array(INPUTS-1 downto 0) of std_logic_vector (31 downto 0);      -- Register-Type for individuals is 24 times 32 bit deep
signal stretch_time : Array32bit:=(others => (others => '0'));                      -- Register Entries     INPUT
signal max_count    : Array32bit:=(others => (others => '0'));                      -- or individual entity INPUT


-- signal reg_individual: std_logic_vector(31 downto 0):=(others => '0');
signal reg_compare_1: std_logic_vector(31 downto 0):=(others => '0');               -- Register to set max value of simultanious peaks
signal disableReg_compare: std_logic_vector(31 downto 0):=(others => '0');          -- Disable Register for single channels

-- registers for READOUT
signal current_count: Array32bit:=(others => (others => '0'));   
-- OUTPUT from indiv to bussystem to log the live values of every channel
signal individual_alerts: std_logic_vector(31 downto 0):=(others => '0');

-- ON / OFF Switch register 32
signal register_onoff :std_logic_vector(31 downto 0):=(others => '0');


-- shutdown Pulse handling
signal signal_register: std_logic_vector(31 downto 0):=(others => '0');
signal shutdown_signal: std_logic;
-- signal shutdown_signal:std_logic_vector(OUTPUTS-1 downto 0);

-- Signals for the Monitoring/Fifo
signal fifo_writesignal: std_logic_vector(INPUTS-1 downto 0);
signal man_readFlag:  std_logic_vector(31 downto 0);
signal read_mem_Flag: std_logic_vector(INPUTS-1 downto 0);
-- signal fifoEmpty: std_logic;
signal fifoEmpty: std_logic_vector(INPUTS-1 downto 0);

-- signal store_values : Array32bit:=(others => (others => '0'));
signal plot_values : Array32bit:=(others => (others => '0'));


-- hand over the signals between entities
signal in_2_indiv   :std_logic_vector(INPUTS-1 downto 0);
signal indiv_2_comp :std_logic_vector(INPUTS-1 downto 0);
signal comp_2_out   :std_logic_vector(OUTPUTS-1 downto 0);

signal address_i      : integer range 0 to 255;







begin

  address_i <= to_integer(unsigned(BUS_RX.addr(7 downto 0)));


  individual_alerts(INPUTS-1 downto 0) <= indiv_2_comp;
  in_2_indiv <= INPUT;
  signal_register(0) <= shutdown_signal; 

  


PROC_OFFSWITCH : process (comp_2_out)
  begin
  if register_onoff(0) = '1' then
    shutdown_signal <= comp_2_out(0);
  else 
    shutdown_signal <= '0'; 
  end if;
end process;
  
  
PROC_REGS : process begin
  wait until rising_edge(CLK);
  BUS_TX.ack     <= '0';
  BUS_TX.nack    <= '0';
  BUS_TX.unknown <= '0';
  
  if BUS_RX.read = '1' then                                                     -- READ 
    BUS_TX.ack <= '1';
    if BUS_RX.addr(11 downto 8) = x"0" then                                 -- Register for individual
          if address_i < INPUTS then
            BUS_TX.data <=  stretch_time(address_i);
          else 
            BUS_TX.ack  <= '0'; BUS_TX.unknown <= '1';
          end if;
    elsif BUS_RX.addr(11 downto 8) = x"1" then
          if address_i < INPUTS then
            BUS_TX.data <=  max_count(address_i);
          else 
            BUS_TX.ack  <= '0'; BUS_TX.unknown <= '1';
          end if;
    elsif BUS_RX.addr(11 downto 8) = x"2" then
            BUS_TX.data <= reg_compare_1;
    elsif BUS_RX.addr(11 downto 8) = x"3" then
            BUS_TX.data <= disableReg_compare;
    elsif BUS_RX.addr(11 downto 8) = x"4" then
          if address_i < INPUTS then
            BUS_TX.data <= current_count(address_i);
          else
            BUS_TX.ack  <= '0'; BUS_TX.unknown <= '1' ;
          end if; 
    elsif BUS_RX.addr(11 downto 8) = x"5" then
            BUS_TX.data <= individual_alerts;
    elsif BUS_RX.addr(11 downto 8) = x"6" then
            BUS_TX.data <= register_onoff;
    elsif BUS_RX.addr(11 downto 8) = x"7" then
            BUS_TX.data <= signal_register;
            
    elsif BUS_RX.addr(11 downto 8) = x"8" then
    
      if address_i < INPUTS then
        if fifoEmpty(address_i) = '1' then
          BUS_TX.nack <= '1';
          BUS_TX.ack  <= '0';
        else
          BUS_TX.data <= plot_values(address_i);
          read_mem_Flag(address_i) <= '1';          
        end if;  
        
      else
        BUS_TX.ack  <= '0'; BUS_TX.unknown <= '1' ;
      end if;
    elsif BUS_RX.addr(11 downto 8) = x"9" then
            BUS_TX.data <= man_readFlag;
    else BUS_TX.ack  <= '0'; BUS_TX.unknown <= '1';       
    end if;  
    
    
  elsif BUS_RX.write = '1' then                                                   --WRITE 
    BUS_TX.ack <= '1';
    if BUS_RX.addr(11 downto 8) = x"0" then  
          if address_i < INPUTS then
            stretch_time(address_i) <= BUS_RX.data;                                 
          else 
            BUS_TX.ack <= '0'; BUS_TX.unknown <= '1';
          end if;
    elsif BUS_RX.addr(11 downto 8) =x"1" then
          if address_i < INPUTS then
            max_count(address_i)  <= BUS_RX.data;                                  -- Write max count values
          else 
            BUS_TX.ack <= '0'; BUS_TX.unknown <= '1';
          end if;          
    elsif BUS_RX.addr(11 downto 8) = x"2" then                                  
            reg_compare_1         <= BUS_RX.data;  
    elsif BUS_RX.addr(11 downto 8) = x"3" then
            disableReg_compare    <= BUS_RX.data;  
    elsif BUS_RX.addr(11 downto 8) = x"6" then                                    -- write the preset Value of ON/OFF in the boards register      
            register_onoff        <= BUS_RX.data;
            
    elsif BUS_RX.addr(11 downto 8) = x"9" then
            man_readFlag     <= BUS_RX.data;         
    else  BUS_TX.ack <= '0'; BUS_TX.unknown <= '1';        
    end if;
  end if;
  
end process;



GEN_INDIV : for i in 0 to INPUTS-1 generate
THE_INDIV: entity work.shutdown_indv

  port map(
  clk_in            => CLK,
  signal_in         => in_2_indiv(i),
  int_time          => stretch_time(i),
  max_count         => max_count(i),
  processed_signal  => indiv_2_comp(i),
  reset_pulse       => fifo_writesignal(i),
  current_hits_vctr => current_count(i)
  );
end generate; 




THE_COMPARE: entity work.shutdown_comp
generic map(
    INPUTS  => INPUTS,
    OUTPUTS => OUTPUTS
)
  port map(
   clk_in             => CLK,
   signal_in          => indiv_2_comp,
   max_number_reg     => reg_compare_1,
   disable_reg        => disableReg_compare,
   processed_signal   => comp_2_out
  );

  
  
  
GEN_MONITOR: for i in 0 to INPUTS-1 generate
THE_MONITOR: entity work.shutdown_mon

  port map(
  clk_in               => CLK,
  reset                => RESET,
  hitvalue             => current_count(i),
  writeFlag            => fifo_writesignal(i),
  read_cmd             => read_mem_Flag(i),                    -- automatic Read Flag
  read_global          => man_readFlag(i),                     -- manuell Read Flag
  output_val           => plot_values(i),
  emptyFlag            => fifoEmpty(i)
  );
end generate;

end architecture;











