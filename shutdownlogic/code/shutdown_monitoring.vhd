library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
library work;
  use work.trb_net_std.all; 


  
entity shutdown_mon is
-- declade all incoming signals
  port(
  clk_in             : in std_logic;
--   disable_reg        : in std_logic;
  reset              : in std_logic;
  hitvalue           : in std_logic_vector (31 downto 0);
  writeFlag          : in std_logic;
  read_cmd           : in std_logic;                           --coming from BUS_RX.read '1'
  read_global        : in std_logic;                           --coming from register/manually
  output_val         : out std_logic_vector (31 downto 0);
  emptyFlag          : out std_logic);
end shutdown_mon;

architecture behave of shutdown_mon is
signal threshValue: std_logic_vector(9 downto 0):= "1111101000"; 
signal data_2_fifo: std_logic_vector (31 downto 0);
signal writesignal: std_logic;
signal readsignal: std_logic:='0';
signal amFull: std_logic;
signal last_amFull: std_logic;
signal allow_write: std_logic;
signal in_2_fifo: std_logic_vector (35 downto 0):= x"000000000";
signal timer: unsigned(15 downto 0):= x"0000";
signal output_val_handler: std_logic_vector (35 downto 0);
-- signal helper: std_logic;

begin
data_2_fifo  <= hitvalue; 
output_val <= output_val_handler(31 downto 0);
in_2_fifo(31 downto 0) <=  hitvalue;
-- allow_write <= not read_global;



PROC_FIFOWRTITE: process is
-- only write if the write flag is active
begin
wait until rising_edge(clk_in);

  if read_global = '0' and writeFlag = '1' then
    writesignal <= '1';
  else 
    writesignal <= '0';
  end if;
  

end process;




PROC_COMBINEDREAD: process is 
begin
wait until rising_edge(clk_in);
  last_amFull <= amFull;
  if allow_write = '0' then
    if read_cmd ='1' then
      readsignal <= '1';
    elsif amFull ='1' and last_amFull='0' then
      readsignal <='1';
    else 
      readsignal <='0';
    end if;
  end if;
end process;

    
    

THE_FIFO: entity work.fifo_36x1k_oreg
  port map(
  Data        =>    in_2_fifo,      --   Data: in  std_logic_vector(35 downto 0); 
  Clock       =>    clk_in,         --   Clock: in  std_logic; 
  WrEn        =>    writesignal,    --   WrEn: in  std_logic; 
  RdEn        =>    readsignal,     --   RdEn: in  std_logic; 
  Reset       =>    reset,          --   Reset: in  std_logic; 
  AmFullThresh=>    threshValue,    --   AmFullThresh: in  std_logic_vector(9 downto 0); 
  Q           =>    output_val_handler,     --   Q: out  std_logic_vector(35 downto 0); 
--   WCNT      =>                    --   WCNT: out  std_logic_vector(10 downto 0); 
  Empty       =>    emptyFlag,
--   Full      =>--   Full: out  std_logic; 
  AlmostFull  =>     amFull   --   AlmostFull: out  std_logic);
  );
end behave;






