library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity envBoardParserHandler is
    port(
        INPUT : in std_logic_vector(7 downto 0);
        CLK   : in std_logic;
        READY : in std_logic;
        
        SERIAL_NUMBER : out std_logic_vector(6 downto 0);
        VALUE_M_T     : out std_logic_vector(30 downto 0);
        VALUE_M_X     : out std_logic_vector(30 downto 0);
        VALUE_M_Y     : out std_logic_vector(30 downto 0);
        VALUE_M_Z     : out std_logic_vector(30 downto 0);
        VALUE_BME_T   : out std_logic_vector(30 downto 0);
        VALUE_BME_P   : out std_logic_vector(30 downto 0);
        VALUE_BME_H   : out std_logic_vector(30 downto 0);
        VALUE_L_0     : out std_logic_vector(30 downto 0);
        VALUE_L_1     : out std_logic_vector(30 downto 0);
        ERROR_NO_DATA : out std_logic;
        DEBUG         : out std_logic_vector(31 downto 0)
    );
end entity;

architecture behavioral of envBoardParserHandler is
    -- signals for THE_MAGBOARD_PARSER
    signal serialNumber  : std_logic_vector(6 downto 0) := "0000000";
    signal sensorNumber  : std_logic_vector(1 downto 0) := "00";
    signal typ           : std_logic_vector(1 downto 0) := "00";
    signal value         : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";
    -- signals for output result from THE_MAGBOARD_PARSER
    -- sensor0
    signal value_M_T_i   : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";
    signal value_M_X_i   : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";
    signal value_M_Y_i   : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";
    signal value_M_Z_i   : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";
    -- sensor1
    signal value_BME_T_i : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";
    signal value_BME_P_i : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";
    signal value_BME_H_i : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";
    -- sensor2
    signal value_L_0_i   : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";
    signal value_L_1_i   : unsigned(30 downto 0) := b"000_0000_0000_0000_0000_0000_0000_0000";


    -- signals for PROC_CHECK_ERROR
    signal error       : std_logic := '0';
    signal counter     : unsigned(28 downto 0);

begin

-- do parsing in external file
THE_ENVBOARD_PARSER : entity work.envBoardParser
  port map(
    --in
    INPUT           => INPUT,
    CLK             => CLK,
    READY           => READY,
    --out
    SERIAL_NUMBER   => serialNumber,
    SENSOR_NUMBER   => sensorNumber,
    TYP_NUMBER      => typ,
    VALUE           => value,
    DEBUG           => DEBUG
  );
  
-- check if there is data to parse
PROC_CHECK_ERROR : process begin
  wait until rising_edge(CLK);
  if READY = '0' then
    counter <= counter + 1;
  else 
    error <= '0';
    counter <= (others => '0');
  end if;
  if counter = 200000000 then
    error   <= '1';
    counter <= (others => '0');
  end if;
end process;

--sort THE_MAGBOARD_PARSER values to signals, so they can be used in output

value_M_T_i   <= value when sensorNumber = "00" and typ = "00";
value_M_X_i   <= value when sensorNumber = "00" and typ = "01";
value_M_Y_i   <= value when sensorNumber = "00" and typ = "10";
value_M_Z_i   <= value when sensorNumber = "00" and typ = "11";

value_BME_T_i <= value when sensorNumber = "01" and typ = "00";
value_BME_P_i <= value when sensorNumber = "01" and typ = "01";
value_BME_H_i <= value when sensorNumber = "01" and typ = "10";

value_L_0_i   <= value when sensorNumber = "10" and typ = "00";
value_L_1_i   <= value when sensorNumber = "10" and typ = "01";


-- write signals to output pins.
SERIAL_NUMBER <= serialNumber;
VALUE_M_T   <= std_logic_vector(value_M_T_i);
VALUE_M_X   <= std_logic_vector(value_M_X_i);
VALUE_M_Y   <= std_logic_vector(value_M_y_i);
VALUE_M_Z   <= std_logic_vector(value_M_Z_i);
VALUE_BME_T <= std_logic_vector(value_BME_T_i);
VALUE_BME_P <= std_logic_vector(value_BME_P_i);
VALUE_BME_H <= std_logic_vector(value_BME_H_i);
VALUE_L_0   <= std_logic_vector(value_L_0_i);
VALUE_L_1   <= std_logic_vector(value_L_1_i);

ERROR_NO_DATA <= error;

end architecture behavioral;
