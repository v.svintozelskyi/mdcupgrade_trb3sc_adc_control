library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.trb_net_std.all;
use work.version.all;

library machxo2;
use machxo2.all;


entity uart_env is
  generic(
    OUTPUTS : integer := 1;
    BAUD    : integer := 19200
    );
  port(
    CLK     : in  std_logic;
    RESET   : in  std_logic; -- reset all values
    UART_RX : in  std_logic_vector(OUTPUTS-1 downto 0); -- incoming data which needs to be processed
    UART_TX : out std_logic_vector(OUTPUTS-1 downto 0); -- send data to board
    
    BUS_RX  : in CTRLBUS_RX; -- what user is sending to FPGA
    BUS_TX  : out CTRLBUS_TX -- what FPGA is sending to user
    );
end entity;


architecture uart_arch of uart_env is

type parserValue_array is array (OUTPUTS - 1 downto 0) of std_logic_vector(30 downto 0);
type serialNumber_array is array (OUTPUTS - 1 downto 0) of std_logic_vector(6 downto 0);
type rxData_array is array (OUTPUTS - 1 downto 0) of std_logic_vector(7 downto 0);

signal rx_data     : rxData_array;
signal rx_ready    : std_logic_vector(OUTPUTS - 1 downto 0);
signal tx_send     : std_logic;
signal tx_ready    : std_logic;
signal out_sel     : integer range 0 to OUTPUTS - 1 := 0;
signal uart_sel_rx : std_logic;
signal uart_sel_tx : std_logic;

signal clk_div     : integer := 100000000/BAUD;

signal tx_fifo_out   : std_logic_vector(8 downto 0);
signal tx_fifo_empty : std_logic;
signal tx_fifo_full  : std_logic;
signal tx_fifo_read  : std_logic;
signal tx_fifo_write : std_logic;

signal next2_tx_send, next_tx_send : std_logic;
signal last2_rx_read, last_rx_read : std_logic;

signal rx_debug : std_logic_vector(3 downto 0);
signal tx_debug : std_logic_vector(3 downto 0);


-- signals for output result from THE_PARSER_HANDLER
signal parser_error  : std_logic_vector(OUTPUTS-  1 downto 0);
signal serialNumbers : serialNumber_array;
-- sensor0
signal value_M_T   : parserValue_array;
signal value_M_X   : parserValue_array;
signal value_M_y   : parserValue_array;
signal value_M_Z   : parserValue_array;
-- sensor1
signal value_BME_T : parserValue_array;
signal value_BME_P : parserValue_array;
signal value_BME_H : parserValue_array;
-- sensor2
signal value_L_0   : parserValue_array;
signal value_L_1   : parserValue_array;

signal debug_i     : std_logic_vector(31 downto 0);
--signal i,j : integer;

begin

GEN_THE_PARSER_HANDLER:
for i in 0 to OUTPUTS - 1 generate
THE_PARSER_HANDLER : entity work.envBoardParserHandler
  port map(
    --in
    INPUT         => rx_data(i),
    CLK           => CLK,
    READY         => rx_ready(i),
    --out
    SERIAL_NUMBER => serialNumbers(i),
    VALUE_M_T     => value_M_T(i),
    VALUE_M_X     => value_M_X(i),
    VALUE_M_Y     => value_M_y(i),
    VALUE_M_Z     => value_M_Z(i),
    VALUE_BME_T   => value_BME_T(i),
    VALUE_BME_P   => value_BME_P(i),
    VALUE_BME_H   => value_BME_H(i),
    VALUE_L_0     => value_L_0(i),
    VALUE_L_1     => value_L_1(i),
    ERROR_NO_DATA => parser_error(i),
    DEBUG         => debug_i
  );
end generate GEN_THE_PARSER_HANDLER;

  

THE_TX_FIFO : entity work.fifo_9x2k_oreg
  port map(
    Clock => CLK,
    Data  => BUS_RX.data(8 downto 0),
    WrEn  => tx_fifo_write,
    RdEn  => tx_fifo_read,
    Reset => RESET,
    Q     => tx_fifo_out,
    Empty => tx_fifo_empty,
    Full  => tx_fifo_full
    );

GEN_THE_RX:
for i in 0 to OUTPUTS - 1 generate
-- transfer incoming signal (uart_sel_rx) in 8bit vector.
THE_RX : entity work.uart_rec
  port map(
    CLK_DIV      => clk_div,
    CLK          => CLK,
    RST          => RESET,
    RX           => UART_RX(i), -- pass signale to sub entity (blackbox)
    DATA_OUT     => rx_data(i), -- return value
    DATA_WAITING => rx_ready(i), -- return value
    DEBUG        => rx_debug -- return value
  );
end generate GEN_THE_RX;

THE_TX : entity work.uart_trans
  port map(
    CLK_DIV      => clk_div,
    CLK          => CLK,
    RST          => RESET,
    DATA_IN      => tx_fifo_out(7 downto 0),
    SEND         => tx_send,
    READY        => tx_ready,
    TX           => uart_sel_tx,
    DEBUG        => tx_debug
  );


PROC_REGS : process 
  variable i,j : integer := 0;
begin
  wait until rising_edge(CLK);
  BUS_TX.unknown <= '0';
  BUS_TX.ack     <= '0';	
  BUS_TX.nack    <= '0';
  BUS_TX.data    <= (others => '0');
  
  tx_fifo_write <= '0';
  --rx_fifo_read  <= '0';
  --last_rx_read  <= rx_fifo_read;
  --last2_rx_read <= last_rx_read;
  
  --if last2_rx_read = '1' then
    --BUS_TX.data(8 downto 0) <= rx_fifo_out;
    --BUS_TX.ack              <= '1';
  --els
  if BUS_RX.write = '1' then
    if BUS_RX.addr(3 downto 0) = x"0" then
      tx_fifo_write <= not tx_fifo_full;
      BUS_TX.ack    <= not tx_fifo_full;
      BUS_TX.nack   <=     tx_fifo_full;
    elsif BUS_RX.addr(3 downto 0) = x"1" then
      clk_div     <= to_integer(unsigned(BUS_RX.data));
      BUS_TX.ack  <= '1';
    elsif BUS_RX.addr(3 downto 0) = x"2" then
      out_sel     <= to_integer(unsigned(BUS_RX.data(3 downto 0)));
      BUS_TX.ack  <= '1';
    else
      BUS_TX.unknown <= '1';
    end if;
  
  elsif BUS_RX.read = '1' then

    i := 1;--to_integer(unsigned(BUS_RX.addr(7 downto 4)));
    j := to_integer(unsigned(BUS_RX.addr(3 downto 0)));
    if i <= OUTPUTS then
      BUS_TX.unknown <= '0';
      BUS_TX.ack  <= '1';
      --if i = 0 then
      --  if j = 0 then
      --    --clock
      --    BUS_TX.data <= std_logic_vector(to_unsigned(clk_div,32));
      --  elsif j <= OUTPUTS then
      --    --serial number of boards and potential error
      --    BUS_TX.data(6 downto 0) <= serialNumbers(j - 1);
      --    BUS_TX.data(8) <= parser_error(j - 1);
      --  else
      --    BUS_TX.unknown <= '1'; BUS_TX.ack  <= '0';
      --  end if;
      --else
        case BUS_RX.addr(3 downto 0) is
          -- sensor Mag: T, X, Y, Z
          when x"0" => BUS_TX.data(27 downto 0) <= value_M_T(i-1)(27 downto 0);
          when x"1" => BUS_TX.data(27 downto 0) <= value_M_X(i-1)(27 downto 0);
          when x"2" => BUS_TX.data(27 downto 0) <= value_M_y(i-1)(27 downto 0);
          when x"3" => BUS_TX.data(27 downto 0) <= value_M_Z(i-1)(27 downto 0);
          -- sensor BME: T, P, H
          when x"4" => BUS_TX.data(27 downto 0) <= value_BME_T(i-1)(27 downto 0);
          when x"5" => BUS_TX.data(27 downto 0) <= value_BME_P(i-1)(27 downto 0);
          when x"6" => BUS_TX.data(27 downto 0) <= value_BME_H(i-1)(27 downto 0);
          -- sensor Light: L0, L1
          when x"7" => BUS_TX.data(27 downto 0) <= value_L_0(i-1)(27 downto 0);
          when x"8" => BUS_TX.data(27 downto 0) <= value_L_1(i-1)(27 downto 0);
          when x"9" => BUS_TX.data <= debug_i;
          when x"A" => BUS_TX.data <= std_logic_vector(to_unsigned(clk_div,32));
          when x"B" => BUS_TX.data(6 downto 0) <= serialNumbers(0);
                       BUS_TX.data(7) <= '0';
                       BUS_TX.data(8) <= parser_error(0);
                       BUS_TX.data(31 downto 9) <= (others => '0');
          when others => BUS_TX.unknown <= '1'; BUS_TX.ack  <= '0';
        end case;
      --end if;
    else 
      BUS_TX.unknown <= '1';
    end if;
  end if;
end process;

    
PROC_SEND : process begin
  wait until rising_edge(CLK);
  tx_fifo_read  <= '0';
  next_tx_send  <= '0';
  next2_tx_send <= next_tx_send;
  tx_send       <= next2_tx_send;
  
  if tx_fifo_empty = '0' and tx_ready = '1' and next_tx_send = '0' and next2_tx_send = '0' then
    next_tx_send <= '1';  
    tx_fifo_read <= '1';
  end if;
end process;
  
proc_io : process begin
  wait until rising_edge(CLK);
  UART_TX(0) <= '1';
end process;

    
end architecture;

